<%-- 
    Document   : index
    Created on : 24-feb-2016, 23.08.25
    Author     : Engineering Ingegneria Informatica S.p.A.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String registrationID = it.eng.oadr.venserver.impl.Oadr20bImpl.getInstance().getRegistrationID();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>VEN</title>
    </head>
    <body>
        <form action="RegisterServlet" id="VenRegisterForm" name="VenRegisterForm" method="POST" >
            <button id="register" type="submit" name="register" >Register</button>
        </form><br/>
        <form action="CancelRegistrationServlet" id="VenCancelRegistrationForm" name="VenCancelRegistrationForm" method="POST" >
            <button id="cancel" type="submit" name="cancel" >Cancel Registration</button>
        </form><br/>
<%
    if (registrationID != null) {
%>        
<p>VEN is registered with registrationID = <%= registrationID %></p>
<%
    }
    else {
%>        
<p>VEN is not registered</p>
<%
    }
%>        
    </body>
</html>
