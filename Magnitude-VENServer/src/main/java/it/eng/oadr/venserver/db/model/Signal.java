package it.eng.oadr.venserver.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.oasis_open.docs.ns.energyinterop._201110.SignalTypeEnumeratedType;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Signal  extends AbstractEntity implements Serializable{
	private static final long serialVersionUID = 2729205987905974613L;
	
	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="itemDescription")
	private String itemDescription;
	
	@Column(name="itemUnits")
	private String itemUnits;
	
	@Column(name="siScaleCode")
	private String siScaleCode;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "event_id")
	private Event event;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "signal", orphanRemoval= true, cascade = CascadeType.ALL)
	private List<IntervalPayload> intervals;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemUnits() {
		return itemUnits;
	}

	public void setItemUnits(String itemUnits) {
		this.itemUnits = itemUnits;
	}

	public String getSiScaleCode() {
		return siScaleCode;
	}

	public void setSiScaleCode(String siScaleCode) {
		this.siScaleCode = siScaleCode;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public List<IntervalPayload> getIntervals() {
		return intervals;
	}

	public void setIntervals(List<IntervalPayload> intervals) {
		this.intervals = intervals;
	}
}
