(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('RegistrationService', [
        '$http',
        'BASE_PATH',
        function ($http, basePath) {
            function infoToParams(info) {
                var params = {};
                params.venName = info.venName;
                params.venId = info.venId;
                params.target = info.target;

                var marketContexts = [];
                if (info.marketContexts) {
                    marketContexts = info.marketContexts.map(function (text) {
                        return { text: text };
                    });
                }
                params.marketContexts = marketContexts;
                return params;
            }

            function paramsToInfo(params) {
                var info = angular.copy(params);
                info.marketContexts = info.marketContexts.map(function (item) {
                    return item.text;
                });
                return info;
            }

            return {
                getInitialProperties: function () {
                    return $http.get(basePath+'/OpenADR2/ven/properties')
                        .then(function (response) {
                            response.data = infoToParams(response.data);
                            return response;
                        });
                },
                queryRegistration: function (serverUrl) {
                    return $http({
                        url: basePath+'/OpenADR2/ven/oadrQueryRegistration',
                        method: 'GET',
                        params: {
                            url: serverUrl
                        }
                    });
                },
                createPartyRegistration: function (params) {
                    var info = paramsToInfo(params);
                    return $http.post(basePath+'/OpenADR2/ven/oadrCreatePartyRegistration', info);
                },
                cancelPartyRegistration: function () {
                    return $http.post(basePath+'/OpenADR2/ven/oadrCancelPartyRegistration');
                },
                startPolling: function (params) {
                    var info = paramsToInfo(params);
                    return $http.post(basePath+'/OpenADR2/ven/oadrStartPolling', info);
                },
                stopPolling: function (params) {
                    return $http.post(basePath+'/OpenADR2/ven/oadrStopPolling');
                },
                togglePolling: function (params) {
                    var info = paramsToInfo(params);
                    return $http.post(basePath+'/OpenADR2/ven/oadrTogglePolling', info);
                }
            };
        }
    ]);
}(this, angular));
