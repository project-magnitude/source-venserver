package it.eng.oadr.venserver.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.google.gson.Gson;

import it.eng.oadr.venserver.db.Dao;
import it.eng.oadr.venserver.db.model.AbstractEntity;

public abstract class AbstractEndpoint<E extends AbstractEntity> {

	private Class<E> clazz;
	protected Dao<E> dao;
	
	public AbstractEndpoint(Class<E> clazz) {
		this.clazz = clazz;
		dao = new Dao<>(clazz);
	}
	
	@POST
	@Consumes("application/json")
	public Response create(String json) {
		E entity = this.parseEntity(json);
		dao.persist(entity);
		return Response.created(
				UriBuilder.fromResource(this.getClass())
						.path(String.valueOf(entity.getId())).build()).build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		E entity = dao.removeById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.noContent().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		E entity = dao.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<E> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		return dao.getAll(startPosition, maxResult);
	}

	protected E parseEntity(String json) {
		Gson gson = new Gson();
		E entity = gson.fromJson(json, this.clazz);
		return entity;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public Response update(@PathParam("id") Long id, String json) {
		E entity = this.parseEntity(json);
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		
		entity = dao.updateById(entity, id);
		return Response.noContent().build();
	}

}
