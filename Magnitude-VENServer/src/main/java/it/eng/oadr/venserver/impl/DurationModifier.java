package it.eng.oadr.venserver.impl;

public class DurationModifier {
	
	private String value;
	
	public DurationModifier(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public static DurationModifier SECONDS = new DurationModifier("S");
	public static DurationModifier MINUTES = new DurationModifier("M");
	public static DurationModifier HOURS = new DurationModifier("H");
}
