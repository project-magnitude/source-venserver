package it.eng.oadr.venserver.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory;

import org.oasis_open.docs.ns.emix._2011._06.power.EnergyItemType;
import org.oasis_open.docs.ns.emix._2011._06.power.EnergyRealType;
import org.oasis_open.docs.ns.emix._2011._06.power.PowerAttributesType;
import org.oasis_open.docs.ns.emix._2011._06.power.PowerItemType;
import org.oasis_open.docs.ns.emix._2011._06.power.PowerRealType;
import org.oasis_open.docs.ns.energyinterop._201110.EiActivePeriodType;
import org.oasis_open.docs.ns.energyinterop._201110.EiEventBaselineType;
import org.oasis_open.docs.ns.energyinterop._201110.EiEventSignalType;
import org.oasis_open.docs.ns.energyinterop._201110.EiResponseType;
import org.oasis_open.docs.ns.energyinterop._201110.EiTargetType;
import org.oasis_open.docs.ns.energyinterop._201110.EventDescriptorType;
import org.oasis_open.docs.ns.energyinterop._201110.IntervalType;
import org.oasis_open.docs.ns.energyinterop._201110.PayloadFloatType;
import org.oasis_open.docs.ns.energyinterop._201110.ReadingTypeEnumeratedType;
import org.oasis_open.docs.ns.energyinterop._201110.ReportEnumeratedType;
import org.oasis_open.docs.ns.energyinterop._201110.ReportSpecifierType;
import org.oasis_open.docs.ns.energyinterop._201110.SignalPayloadType;
import org.openadr.oadr_2_0b._2012._07.BaseUnitType;
import org.openadr.oadr_2_0b._2012._07.CurrencyType;
import org.openadr.oadr_2_0b._2012._07.OadrCancelOptType;
import org.openadr.oadr_2_0b._2012._07.OadrCancelPartyRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrCancelReportType;
import org.openadr.oadr_2_0b._2012._07.OadrCanceledOptType;
import org.openadr.oadr_2_0b._2012._07.OadrCanceledPartyRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrCanceledReportType;
import org.openadr.oadr_2_0b._2012._07.OadrCreateOptType;
import org.openadr.oadr_2_0b._2012._07.OadrCreatePartyRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrCreateReportType;
import org.openadr.oadr_2_0b._2012._07.OadrCreatedEventType;
import org.openadr.oadr_2_0b._2012._07.OadrCreatedOptType;
import org.openadr.oadr_2_0b._2012._07.OadrCreatedPartyRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrCreatedReportType;
import org.openadr.oadr_2_0b._2012._07.OadrDistributeEventType;
import org.openadr.oadr_2_0b._2012._07.OadrDistributeEventType.OadrEvent;
import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrPendingReportsType;
import org.openadr.oadr_2_0b._2012._07.OadrPollType;
import org.openadr.oadr_2_0b._2012._07.OadrQueryRegistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrRegisterReportType;
import org.openadr.oadr_2_0b._2012._07.OadrRegisteredReportType;
import org.openadr.oadr_2_0b._2012._07.OadrReportDescriptionType;
import org.openadr.oadr_2_0b._2012._07.OadrReportPayloadType;
import org.openadr.oadr_2_0b._2012._07.OadrReportRequestType;
import org.openadr.oadr_2_0b._2012._07.OadrReportType;
import org.openadr.oadr_2_0b._2012._07.OadrRequestEventType;
import org.openadr.oadr_2_0b._2012._07.OadrRequestReregistrationType;
import org.openadr.oadr_2_0b._2012._07.OadrResponseType;
import org.openadr.oadr_2_0b._2012._07.OadrSamplingRateType;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.openadr.oadr_2_0b._2012._07.OadrTransportType;
import org.openadr.oadr_2_0b._2012._07.OadrUpdateReportType;
import org.openadr.oadr_2_0b._2012._07.OadrUpdatedReportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ietf.params.xml.ns.icalendar_2.Dtstart;
import ietf.params.xml.ns.icalendar_2.DurationPropType;
import ietf.params.xml.ns.icalendar_2_0.stream.Intervals;
import it.eng.oadr.openadrclient.Oadr20bClient;
import it.eng.oadr.openadrclient.Oadr20bClient.PATH;
import it.eng.oadr.openadrmodel.Parser;
import it.eng.oadr.venserver.db.Dao;
import it.eng.oadr.venserver.db.model.Event;
import it.eng.oadr.venserver.db.model.IntervalPayload;
import it.eng.oadr.venserver.db.model.Log;
import it.eng.oadr.venserver.db.model.ReportRequest;
import it.eng.oadr.venserver.db.model.Resource;
import it.eng.oadr.venserver.db.model.Signal;
import it.eng.oadr.venserver.db.model.StorageResource;
import it.eng.oadr.venserver.model.RegistrationInfo;
import it.eng.oadr.venserver.model.ReplyMessage;
import it.eng.oadr.venserver.socket.SocketServer;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class Oadr20bImpl {

	private static final Logger log = LoggerFactory.getLogger(Oadr20bImpl.class);
	public static String SCHEMA_VERSION = null;
	private static Oadr20bImpl INSTANCE = null;
	private static int NUM_DIGIT_REQUEST_ID = 10;
	public static String VEN_ID = null;
	// public static String MARKET_CONTEXT = null;
	public static String VEN_NAME = null;
	public static String VEN_TRANSPORT_ADDRESS = null;
	public static String VEN_PULL_MODEL = null;
	public static OadrCreatedPartyRegistrationType REGISTRATION = null;
	public static Timer TIMER = null;
	// public static ArrayList<String> LIST_REPORT_SPECIFIER_ID = null;
	public static ArrayList<String> LIST_REPORT_REQUEST_ID = null;
	public static ArrayList<String> LIST_MARKET_CONTEXT = null;

	private static ExecutorService es;
	public static Map<String, Queue<OadrDistributeEventType>> mapQueueOadrDistributeEvent = null;
	public static Map<String, Map<String, OadrDistributeEventType>> mapMapOadrDistributeEvent = null;
	public static Map<String, Queue<OadrCreateReportType>> mapQueueOadrCreateReport = null;
	public static Map<String, Map<String, OadrCreateReportType>> mapMapOadrCreateReport = null;
	public static Queue<OadrUpdateReportType> mapQueueOadrUpdateReport = null;
	public static Map<String, String> mapReportSpecificerMarketContext = null;
	private static long pollFreq = 0;

	private RegistrationInfo registrationInfo;
	private boolean registeredReport = false;
	private boolean isPollRunning = false;

	private Oadr20bImpl() {
		try {
			VEN_ID = VenProperties.getProperty("venID");
			// VEN_ID = generateID(20);
			SCHEMA_VERSION = VenProperties.getProperty("schemaVersion");
			VEN_NAME = VenProperties.getProperty("venName");
			VEN_TRANSPORT_ADDRESS = VenProperties.getProperty("venTransportAddress");
			VEN_PULL_MODEL = VenProperties.getProperty("pullModel");

			String propmarketContextList = VenProperties.getProperty("marketContextList");
			String delims = "[,\\n]";
			String[] tokens = propmarketContextList.split(delims);
			log.debug("tokens: " + tokens.toString());
			LIST_MARKET_CONTEXT = new ArrayList<String>(Arrays.asList(tokens));

			// LIST_REPORT_SPECIFIER_ID = new ArrayList<String>();
			LIST_REPORT_REQUEST_ID = new ArrayList<String>();
			mapReportSpecificerMarketContext = new HashMap<String, String>();

			registrationInfo = new RegistrationInfo();
			registrationInfo.setVenUrl(VEN_TRANSPORT_ADDRESS);
			registrationInfo.setVenName(VEN_NAME);
			registrationInfo.setVenId(VEN_ID);
			registrationInfo.setTarget(getTarget());
			registrationInfo.setMarketContexts(LIST_MARKET_CONTEXT);

			log.debug("VEN_ID: " + VEN_ID);
			log.debug("SCHEMA_VERSION: " + SCHEMA_VERSION);
			log.debug("VEN_NAME: " + VEN_NAME);
			log.debug("VEN_TRANSPORT_ADDRESS: " + VEN_TRANSPORT_ADDRESS);
			log.debug("VEN_PULL_MODEL: " + VEN_PULL_MODEL);
			log.debug("LIST_MARKET_CONTEXT: " + LIST_MARKET_CONTEXT.toString());
			es = Executors.newSingleThreadExecutor();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
	}

	public synchronized static Oadr20bImpl getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Oadr20bImpl();
		}
		return INSTANCE;
	}

	public String getRegistrationID() {
		String ret = null;
		if (REGISTRATION != null) {
			ret = REGISTRATION.getRegistrationID();
		}
		return ret;
	}

	public long getPollFreq() {
		return pollFreq;
	}

	public String getTarget() {
		String target = "";
		try {
			target = Oadr20bClient.getInstance().getTarget();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return target;
	}

	public void setTarget(String target) {
		try {
			Oadr20bClient.getInstance().setTarget(target);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public RegistrationInfo getRegistrationInfo() {
		return this.registrationInfo;
	}

	public void setRegistrationInfo(RegistrationInfo registrationInfo) {
		this.registrationInfo = registrationInfo;
		if (this.registrationInfo == null) {
			return;
		}

		VEN_NAME = this.registrationInfo.getVenName();
		VEN_ID = this.registrationInfo.getVenId();
		LIST_MARKET_CONTEXT = (ArrayList<String>) this.registrationInfo.getMarketContexts();
		setTarget(this.registrationInfo.getTarget());
	}

	public synchronized void shutdown() {
		if (es != null) {
			es.shutdown();
		}

	}

	private void saveEvents(List<OadrEvent> events) {
		Event entityEvent;
		Dao<Event> daoEvent = new Dao<>(Event.class);

		for (OadrEvent oadrEvent : events) {
			entityEvent = new Event();
			EventDescriptorType descriptor = oadrEvent.getEiEvent().getEventDescriptor();
			entityEvent.setEventId(descriptor.getEventID());
			entityEvent.setModificationNumber(descriptor.getModificationNumber());
			entityEvent.setPriority(descriptor.getPriority());
			entityEvent.setMarketContext(descriptor.getEiMarketContext().getMarketContext());
			entityEvent.setCreatedDate(descriptor.getCreatedDateTime().toGregorianCalendar().getTime());
			entityEvent.setEventStatus(descriptor.getEventStatus().value());
			entityEvent.setVtnComment(descriptor.getVtnComment());

			EiActivePeriodType activePeriod = oadrEvent.getEiEvent().getEiActivePeriod();
			entityEvent.setStartTime(
					activePeriod.getProperties().getDtstart().getDateTime().toGregorianCalendar().getTime());
			entityEvent.setDuration(activePeriod.getProperties().getDuration().getDuration());
			entityEvent.setStartAfter(activePeriod.getProperties().getTolerance().getTolerate().getStartafter());
			entityEvent.setEiNotification(activePeriod.getProperties().getXEiNotification().getDuration());
			entityEvent.setEiRampUp(activePeriod.getProperties().getXEiRampUp().getDuration());
			entityEvent.setEiRecovery(activePeriod.getProperties().getXEiRecovery().getDuration());

			entityEvent.setResponseRequired(oadrEvent.getOadrResponseRequired().value());

			// get baseline from message
			Signal entitySignal;
			ArrayList<Signal> newSignals = new ArrayList<Signal>();
			EiEventBaselineType baseline = oadrEvent.getEiEvent().getEiEventSignals().getEiEventBaseline();
			if (baseline != null) {
				entitySignal = new Signal();
				entitySignal.setName(baseline.getBaselineName());
				entitySignal.setType("baseline");
				entitySignal = setItemIntervals(entitySignal, baseline.getItemBase(),
						baseline.getIntervals().getInterval());
				entitySignal.setEvent(entityEvent);
				newSignals.add(entitySignal);
			}

			// get signals
			List<EiEventSignalType> oadrSignals = oadrEvent.getEiEvent().getEiEventSignals().getEiEventSignal();
			for (EiEventSignalType oadrSignal : oadrSignals) {
				entitySignal = new Signal();
				entitySignal.setName(oadrSignal.getSignalName());
				entitySignal.setType(oadrSignal.getSignalType().value());
				entitySignal.setEvent(entityEvent);

				entitySignal = setItemIntervals(entitySignal, oadrSignal.getItemBase(),
						oadrSignal.getIntervals().getInterval());
				log.debug("intervals size " + entitySignal.getIntervals().size());
				newSignals.add(entitySignal);
			}
			entityEvent.setSignals(newSignals);

			List<Event> list = daoEvent.findByField("eventId", entityEvent.getEventId());
			if (!list.isEmpty()) {
				Event oldEvent = list.get(0);
				entityEvent.setId(oldEvent.getId());
				daoEvent.updateById(entityEvent, oldEvent.getId());
			} else {
				daoEvent.persist(entityEvent);
			}
			SocketServer.INSTANCE.broadcast(SocketServer.EVENT_NAME.EVENT, entityEvent);
		}
	}

	private Signal setItemIntervals(Signal signal, JAXBElement itemBase, List<IntervalType> intervals) {
		if (itemBase != null) {
			if (itemBase.getValue() instanceof EnergyItemType) {
				EnergyItemType e = (EnergyItemType) itemBase.getValue();
				signal.setItemDescription(e.getItemDescription());
				signal.setItemUnits(e.getItemUnits());
				signal.setSiScaleCode(e.getSiScaleCode());
			}
			if (itemBase.getValue() instanceof PowerItemType) {
				PowerItemType p = (PowerItemType) itemBase.getValue();
				signal.setItemDescription(p.getItemDescription());
				signal.setItemUnits(p.getItemUnits());
				signal.setSiScaleCode(p.getSiScaleCode());
			}
		}
		IntervalPayload entityInterval;
		List<IntervalPayload> newIntervals = new ArrayList<IntervalPayload>();
		for (IntervalType oadrInterval : intervals) {
			entityInterval = new IntervalPayload();
			entityInterval.setUid(Integer.parseInt(oadrInterval.getUid().getText()));
			JAXBElement<SignalPayloadType> signalPayloadType = (JAXBElement<SignalPayloadType>) oadrInterval
					.getStreamPayloadBase().get(0);
			JAXBElement<PayloadFloatType> payloadFloatType = (JAXBElement<PayloadFloatType>) signalPayloadType
					.getValue().getPayloadBase();
			float value = payloadFloatType.getValue().getValue();
			entityInterval.setValue(value);
			entityInterval.setSignal(signal);
			newIntervals.add(entityInterval);
		}

		signal.setIntervals(newIntervals);
		return signal;
	}

	private void saveReportRequests(List<OadrReportRequestType> oadrRequests) {
		ReportRequest entity;
		Dao<ReportRequest> dao = new Dao<>(ReportRequest.class);
		for (OadrReportRequestType request : oadrRequests) {
			entity = new ReportRequest();
			entity.setRequestId(request.getReportRequestID());
			ReportSpecifierType specifier = request.getReportSpecifier();
			entity.setSpecifierId(specifier.getReportSpecifierID());
			entity.setGranularity(specifier.getGranularity().getDuration());
			entity.setReportBackDuration(specifier.getReportBackDuration().getDuration());
			entity.setIntervalStart(specifier.getReportInterval().getProperties().getDtstart().getDateTime()
					.toGregorianCalendar().getTime());
			entity.setIntervalDuration(specifier.getReportInterval().getProperties().getDuration().getDuration());
			dao.persist(entity);
			SocketServer.INSTANCE.broadcast(SocketServer.EVENT_NAME.REPORT_REQUEST, entity);
		}
	}

	private OadrPayload postOadrMessage(PATH path, OadrPayload reqPayload) throws JAXBException, Exception {
		Log entity = new Log();
		Date now = new Date();
		entity.setDate(now);

		String reqString = Parser.marshal(reqPayload);
		entity.setRequestPayload(reqString);

		OadrPayload resPayload = Oadr20bClient.getInstance().post(path, reqPayload);

		Date end = new Date();

		ReplyMessage message = new ReplyMessage(reqPayload, resPayload);

		entity.setRequestType(message.getRequestType());
		if (message.getResponseCode() != null) {
			entity.setResponseCode(Long.parseLong(message.getResponseCode()));
		}
		entity.setResponseMessage(message.getResponseMessage());
		String resString = Parser.marshal(resPayload);
		entity.setResponsePayload(resString);
		float seconds = (float) (end.getTime() - now.getTime()) / (float) 1000;

		entity.setResponseTime(seconds);
		entity.setResponseType(message.getResponseType());

		Dao<Log> dao = new Dao<>(Log.class);
		// scartiamo i messaggi di Polling
		if (!entity.getRequestType().equals("oadrPoll")) {
			dao.persist(entity);

			message.setId(entity.getId());
			message.setDate(entity.getDate());
			message.setResponseTime(entity.getResponseTime());

			SocketServer.INSTANCE.broadcast(SocketServer.EVENT_NAME.POLLING, message);
		}
		return resPayload;
	}

//	public void onOadrDistributeEvent(OadrDistributeEventType in) {
	public OadrResponseType onOadrDistributeEvent(OadrDistributeEventType in) {
		log.debug("onOadrDistributeEvent");

		saveEvents(in.getOadrEvent());

		// es.execute(new OadrDistributeEventRunnable(in));

		// estrae il campo marketContext dal payload
		// oadrPayload:oadrSignedObject:oadrDistributeEvent:
		// oadrEvent:eiEvent:eventDescriptor:eiMarketContext:marketContext

		String myMarketContext = in.getOadrEvent().get(0).getEiEvent().getEventDescriptor().getEiMarketContext()
				.getMarketContext();
		log.debug("myMarketContext: " + myMarketContext);

		log.debug("LIST_MARKET_CONTEXT: " + LIST_MARKET_CONTEXT.toString());

		// controlla che myMarketContext sia presente in LIST_MARKET_CONTEXT
		if (LIST_MARKET_CONTEXT.contains(myMarketContext)) {

			// se non esiste la coda con key (<marketContext>,"OadrDistributeEvent",
			// "QUEUE")
			// crea una coda FIFO

			if (mapQueueOadrDistributeEvent == null) {
				mapQueueOadrDistributeEvent = new HashMap<String, Queue<OadrDistributeEventType>>();
			}

			if (mapQueueOadrDistributeEvent.get(myMarketContext) == null) {
				LinkedList<OadrDistributeEventType> queueOadrDistributeEvent = new LinkedList<OadrDistributeEventType>();
				mapQueueOadrDistributeEvent.put(myMarketContext, queueOadrDistributeEvent);
			}

			// inserisce in coda il payload corrente OadrDistributeEventType in coda FIFO
			// individuata
			//
			mapQueueOadrDistributeEvent.get(myMarketContext).add(in);
			log.debug("add in Queue - key:" + myMarketContext);
			log.debug("add in Queue - value: " + in);
			// invia al VTN risposta HTTP 200 OK
		}
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrRegisteredReportType onOadrRegisterReport(OadrRegisterReportType in) {
		log.debug("onOadrRegisterReport");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(in.getRequestID());
		OadrRegisteredReportType ret = new OadrRegisteredReportType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrCreatedReportType onOadrCreateReport(OadrCreateReportType in) {
		log.debug("onOadrCreateReport");

		// inserisce in cosa l'oggetto OadrCreateReportType con key (
		//
		// public static Map<String, Queue<OadrCreateReportType>>
		// mapQueueOadrCreateReport = null;
		// public static Map<String, Map<String, OadrCreateReportType>>
		// mapMapOadrCreateReport = null;
		// public static Map<String, String> mapReportSpecificerMarketContext = null;

		if (!in.getOadrReportRequest().isEmpty()) {
			String myReportSpecifier = in.getOadrReportRequest().get(0).getReportSpecifier().getReportSpecifierID();
			if (mapReportSpecificerMarketContext.containsKey(myReportSpecifier)) {
				String myMarketContext = mapReportSpecificerMarketContext.get(myReportSpecifier);

				if (mapQueueOadrCreateReport == null) {
					mapQueueOadrCreateReport = new HashMap<String, Queue<OadrCreateReportType>>();
				}

				if (mapQueueOadrCreateReport.get(myMarketContext) == null) {
					LinkedList<OadrCreateReportType> queueOadrCreateReport = new LinkedList<OadrCreateReportType>();
					mapQueueOadrCreateReport.put(myMarketContext, queueOadrCreateReport);
				}

				// inserisce in coda il payload corrente OadrCreateReportType in coda FIFO
				// individuata

				mapQueueOadrCreateReport.get(myMarketContext).add(in);
				log.debug("add in Queue - key:" + myMarketContext);
				log.debug("add in Queue - value: " + in);
			}
		}

		// salvo i report request nel db
		saveReportRequests(in.getOadrReportRequest());

		// ritorna un oggetto OadrCreatedReportType
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(in.getRequestID());
		OadrCreatedReportType ret = new OadrCreatedReportType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		for (Iterator<OadrReportRequestType> iterator = in.getOadrReportRequest().iterator(); iterator.hasNext();) {
			OadrReportRequestType reportRequest = iterator.next();
			String myReportRequestID = reportRequest.getReportRequestID();
			OadrPendingReportsType myOadrPendingReportsType = new OadrPendingReportsType();
			myOadrPendingReportsType.getReportRequestID().add(myReportRequestID);
			ret.setOadrPendingReports(myOadrPendingReportsType);
			LIST_REPORT_REQUEST_ID.add(myReportRequestID);
		}
		return ret;
	}

	public OadrUpdatedReportType onOadrUpdateReport(OadrUpdateReportType in) {
//		log.debug("onOadrUpdateReport");
//		EiResponseType eiResponse = new EiResponseType();
//		eiResponse.setResponseCode("200");
//		eiResponse.setResponseDescription("OK");
//		eiResponse.setRequestID(in.getRequestID());
//		OadrUpdatedReportType ret = new OadrUpdatedReportType();
//		ret.setEiResponse(eiResponse);
//		ret.setSchemaVersion(SCHEMA_VERSION);
//		ret.setVenID(VEN_ID);
//
//    	return ret;

		if (in.getOadrReport() != null) {

			if (mapQueueOadrUpdateReport == null) {
				mapQueueOadrUpdateReport = new LinkedList<OadrUpdateReportType>();
			}
			mapQueueOadrUpdateReport.add(in);
			log.debug("add in Queue - value: " + in);
		}

		// ritorna un oggetto OadrUpdatedReportType
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(in.getRequestID());
		OadrUpdatedReportType ret = new OadrUpdatedReportType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrCanceledReportType onOadrCancelReport(OadrCancelReportType in) {
		log.debug("onOadrCancelReport");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(in.getRequestID());
		OadrCanceledReportType ret = new OadrCanceledReportType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);

		if (in.isReportToFollow()) {
			OadrPendingReportsType myOadrPendingReportsType = new OadrPendingReportsType();
			myOadrPendingReportsType.getReportRequestID().addAll(LIST_REPORT_REQUEST_ID);
			ret.setOadrPendingReports(myOadrPendingReportsType);
		} else {
			OadrPendingReportsType myOadrPendingReportsType = new OadrPendingReportsType();
			ret.setOadrPendingReports(myOadrPendingReportsType);

			for (Iterator<String> iterator = in.getReportRequestID().iterator(); iterator.hasNext();) {
				String myReportRequestID = iterator.next();
				LIST_REPORT_REQUEST_ID.remove(myReportRequestID);
			}

		}

		return ret;
	}

	public OadrCanceledPartyRegistrationType onOadrCancelPartyRegistration(OadrCancelPartyRegistrationType in) {
		log.debug("onOadrCancelPartyRegistration");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		eiResponse.setRequestID(in.getRequestID());
		OadrCanceledPartyRegistrationType ret = new OadrCanceledPartyRegistrationType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		if (REGISTRATION != null) {
			ret.setRegistrationID(REGISTRATION.getRegistrationID());
		} else {
			ret.setRegistrationID(in.getRegistrationID());
		}
		REGISTRATION = null;
		return ret;
	}

	public OadrResponseType onOadrCreatedPartyRegistration(OadrCreatedPartyRegistrationType in) {
		log.debug("onOadrCreatedPartyRegistration");

		try {
			// riceve in risposta l'oggetto OadrCreatedPartyRegistrationType
			log.debug("in: " + in);
			Duration duration = DatatypeFactory.newInstance()
					.newDuration(in.getOadrRequestedOadrPollFreq().getDuration());
			pollFreq = duration.getTimeInMillis(new Date());
			log.debug("pollFreq: " + pollFreq);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		REGISTRATION = in;

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		// es.execute(new OadrRequestReregistrationRunnable());
		return ret;
	}

	public OadrResponseType onOadrCanceledPartyRegistration(OadrCanceledPartyRegistrationType in) {
		log.debug("onOadrCanceledPartyRegistration");

		REGISTRATION = null;

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		// es.execute(new OadrRequestReregistrationRunnable());
		return ret;
	}

	public OadrResponseType onOadrCanceledOpt(OadrCanceledOptType in) {
		log.debug("onOadrCanceledOpt");

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrResponseType onOadrCreatedOpt(OadrCreatedOptType in) {
		log.debug("onOadrCreatedOpt");

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrResponseType onOadrPoll(OadrPollType in) {
		log.debug("onOadrPoll");

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		return ret;
	}

	public OadrResponseType onOadrRegisteredReport(OadrRegisteredReportType in) {
		log.debug("onOadrRegisteredReport");

		registeredReport = true;
		saveReportRequests(in.getOadrReportRequest());
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		// es.execute(new OadrRequestReregistrationRunnable());
		return ret;
	}

	public OadrResponseType onOadrUpdatedReport(OadrUpdatedReportType in) {
		log.debug("onOadrUpdatedReport");

		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		// es.execute(new OadrRequestReregistrationRunnable());
		return ret;
	}

	public OadrResponseType onOadrRequestReregistration(OadrRequestReregistrationType in) {
		log.debug("onOadrRequestReregistration");
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType ret = new OadrResponseType();
		ret.setEiResponse(eiResponse);
		ret.setSchemaVersion(SCHEMA_VERSION);
		ret.setVenID(VEN_ID);
		doCreatePartyRegistration();
		// es.execute(new OadrRequestReregistrationRunnable());
		return ret;
	}

	public OadrResponseType sendOadrCreatedEvent(OadrCreatedEventType in) throws Exception {
		log.debug("sendOadrCreatedEvent");
		in.setSchemaVersion(SCHEMA_VERSION);
		in.getEiCreatedEvent().setVenID(VEN_ID);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCreatedEvent(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_EVENT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	// public OadrDistributeEventType sendOadrRequestEvent(OadrRequestEventType in)
	// throws Exception {
	public OadrPayload sendOadrRequestEvent(OadrRequestEventType in) throws Exception {
		log.debug("sendOadrRequestEvent");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrRequestEvent(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_EVENT, pld);
		// return ret.getOadrSignedObject().getOadrDistributeEvent();
		return ret;
	}

	// public OadrRegisteredReportType sendOadrRegisterReport(OadrRegisterReportType
	// in) throws Exception {
	public OadrPayload sendOadrRegisterReport(OadrRegisterReportType in) throws Exception {
		log.debug("sendOadrRegisterReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrRegisterReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		// return ret.getOadrSignedObject().getOadrRegisteredReport();
		return ret;
	}

	public OadrCreatedReportType sendOadrCreateReport(OadrCreateReportType in) {
		return null;
	}

	public OadrUpdatedReportType sendOadrUpdateReport(OadrUpdateReportType in) throws Exception {
		log.debug("sendOadrUpdateReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		in.setVenID(VEN_ID);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrUpdateReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrUpdatedReport();
	}

	public OadrCanceledReportType sendOadrCancelReport(OadrCancelReportType in) throws Exception {
		log.debug("sendOadrCancelReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCancelReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrCanceledReport();
	}

	// public OadrCreatedPartyRegistrationType
	// sendOadrQueryRegistration(OadrQueryRegistrationType in) throws Exception {
	public OadrPayload sendOadrQueryRegistration(OadrQueryRegistrationType in) throws Exception {
		log.debug("sendOadrQueryRegistration");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrQueryRegistration(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REGISTER_PARTY, pld);
		// return ret.getOadrSignedObject().getOadrCreatedPartyRegistration();
		return ret;
	}

	// public OadrCreatedPartyRegistrationType
	// sendOadrCreatePartyRegistration(OadrCreatePartyRegistrationType in)
	public OadrPayload sendOadrCreatePartyRegistration(OadrCreatePartyRegistrationType in) throws Exception {
		log.debug("sendOadrCreatePartyRegistration");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCreatePartyRegistration(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REGISTER_PARTY, pld);
		// return ret.getOadrSignedObject().getOadrCreatedPartyRegistration();
		return ret;
	}

	// public OadrCanceledPartyRegistrationType
	// sendOadrCancelPartyRegistration(OadrCancelPartyRegistrationType in)
	public OadrPayload sendOadrCancelPartyRegistration(OadrCancelPartyRegistrationType in) throws Exception {
		log.debug("sendOadrCancelPartyRegistration");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCancelPartyRegistration(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REGISTER_PARTY, pld);
		// return ret.getOadrSignedObject().getOadrCanceledPartyRegistration();
		return ret;
	}

	public OadrResponseType sendOadrCanceledPartyRegistration(OadrCanceledPartyRegistrationType in) throws Exception {
		log.debug("sendOadrCanceledPartyRegistration");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCanceledPartyRegistration(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REGISTER_PARTY, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrCanceledReport(OadrCanceledReportType in) throws Exception {
		log.debug("sendOadrCanceledReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCanceledReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrRegisteredReport(OadrRegisteredReportType in) throws Exception {
		log.debug("sendOadrRegisteredReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrRegisteredReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrCreatedReport(OadrCreatedReportType in) throws Exception {
		log.debug("sendOadrCreatedReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCreatedReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrUpdatedReport(OadrUpdatedReportType in) throws Exception {
		log.debug("sendOadrUpdatedReport");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrUpdatedReport(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_REPORT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrCreateOpt(OadrCreateOptType in) throws Exception {
		log.debug("sendOadrCreateOpt");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCreateOpt(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_OPT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrResponseType sendOadrCancelOpt(OadrCancelOptType in) throws Exception {
		log.debug("sendOadrCancelOpt");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrCancelOpt(in);
		OadrPayload ret = this.postOadrMessage(Oadr20bClient.PATH.EI_OPT, pld);
		return ret.getOadrSignedObject().getOadrResponse();
	}

	public OadrSignedObject sendOadrPoll(OadrPollType in) throws Exception {
		log.debug("sendOadrPoll");
		in.setSchemaVersion(SCHEMA_VERSION);
		OadrPayload pld = new OadrPayload();
		pld.setOadrSignedObject(new OadrSignedObject());
		pld.getOadrSignedObject().setOadrPoll(in);
		OadrPayload outPld = this.postOadrMessage(Oadr20bClient.PATH.OADR_POLL, pld);
		OadrSignedObject outSiOb = outPld.getOadrSignedObject();
		if (outPld != null && outPld.getOadrSignedObject() != null) {
			outSiOb = outPld.getOadrSignedObject();
			if (outSiOb.getOadrResponse() != null) {
				// log.debug("OadrResponse: " + outSiOb);

			}
			if (outSiOb.getOadrDistributeEvent() != null) {
				log.debug("OadrDistributeEvent: " + outSiOb);
				onOadrDistributeEvent(outSiOb.getOadrDistributeEvent());

			}
			if (outSiOb.getOadrCancelPartyRegistration() != null) {
				log.debug("OadrCancelPartyRegistration: " + outSiOb);
				sendOadrCanceledPartyRegistration(
						onOadrCancelPartyRegistration(outSiOb.getOadrCancelPartyRegistration()));

			}
			if (outSiOb.getOadrRequestReregistration() != null) {
				log.debug("getOadrRequestReregistration: " + outSiOb);
				onOadrRequestReregistration(outSiOb.getOadrRequestReregistration());
			}
			if (outSiOb.getOadrCancelReport() != null) {
				log.debug("getOadrCancelReport: " + outSiOb);
				sendOadrCanceledReport(onOadrCancelReport(outSiOb.getOadrCancelReport()));
			}
			if (outSiOb.getOadrRegisterReport() != null) {
				log.debug("getOadrRegisterReport: " + outSiOb);
				sendOadrRegisteredReport(onOadrRegisterReport(outSiOb.getOadrRegisterReport()));
			}
			if (outSiOb.getOadrCreateReport() != null) {
				log.debug("getOadrCreateReport: " + outSiOb);
				sendOadrCreatedReport(onOadrCreateReport(outSiOb.getOadrCreateReport()));
			}
			if (outSiOb.getOadrUpdateReport() != null) {
				log.debug("getOadrUpdateReport: " + outSiOb);
				sendOadrUpdatedReport(onOadrUpdateReport(outSiOb.getOadrUpdateReport()));
			}
		}

//        if (in != null && in.getOadrSignedObject() != null) {
//            OadrSignedObject so = in.getOadrSignedObject();
//            if (so.getOadrCancelPartyRegistration() != null) {
//                out.getOadrSignedObject().setOadrCanceledPartyRegistration(Oadr20bImpl.getInstance().onOadrCancelPartyRegistration(so.getOadrCancelPartyRegistration()));
//            }
//            else if (so.getOadrRequestReregistration() != null) {
//                out.getOadrSignedObject().setOadrResponse(Oadr20bImpl.getInstance().onOadrRequestReregistration(so.getOadrRequestReregistration()));
//            }
//        }

		return outSiOb;
	}

	public void registerVEN() throws Exception {
		OadrCreatePartyRegistrationType oCrPaRe = new OadrCreatePartyRegistrationType();
		oCrPaRe.setRequestID("1234567890B");
		oCrPaRe.setOadrProfileName("2.0b");
		oCrPaRe.setOadrTransportName(OadrTransportType.SIMPLE_HTTP);
		oCrPaRe.setOadrReportOnly(false);
		oCrPaRe.setOadrXmlSignature(false);
		oCrPaRe.setOadrVenName(VEN_NAME);
		oCrPaRe.setVenID(VEN_ID);
		oCrPaRe.setOadrHttpPullModel(Boolean.FALSE);
		// oCrPaRe.setOadrHttpPullModel(Boolean.TRUE);
		oCrPaRe.setOadrTransportAddress(VEN_TRANSPORT_ADDRESS);
		if (REGISTRATION != null) {
			oCrPaRe.setRegistrationID(REGISTRATION.getRegistrationID());
		}
		// OadrCreatedPartyRegistrationType oCredPaRe =
		// sendOadrCreatePartyRegistration(oCrPaRe);
		OadrPayload oCredPaRe = sendOadrCreatePartyRegistration(oCrPaRe);
//		REGISTRATION = oCredPaRe;
//		log.debug("REGISTRATION_ID = " + REGISTRATION.getRegistrationID());

		OadrRegisterReportType oReRe = new OadrRegisterReportType();
		oReRe.setRequestID("1234567890C");
		oReRe.setVenID(VEN_ID);
		// OadrRegisteredReportType oReedRe = sendOadrRegisterReport(oReRe);
		OadrPayload oReedRe = sendOadrRegisterReport(oReRe);
	}

	public void doQueryRegistration() {
		log.debug("--> doQueryRegistration");
		// crea un oggetto OadrQueryRegistrationType
		OadrQueryRegistrationType oQuRe = new OadrQueryRegistrationType();
		// imposta i campi di OadrQueryRegistrationType
		// utilizza un ID di 20 cifre esadecimali
		oQuRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
		// imposta lo schema Version
		oQuRe.setSchemaVersion(SCHEMA_VERSION);
		// invia l'oggetto OadrQueryRegistrationType

		// OadrCreatedPartyRegistrationType oCredPaRe = null;
		OadrPayload oCredPaRe = null;
		try {
			// riceve in risposta l'oggetto OadrCreatedPartyRegistrationType
			log.debug("oQuRe: " + oQuRe);
			oCredPaRe = sendOadrQueryRegistration(oQuRe);
			// lo processa
			// salva la duration per il poll
//			Duration duration = DatatypeFactory.newInstance()
//					.newDuration(oCredPaRe.getOadrRequestedOadrPollFreq().getDuration());
//			pollFreq = duration.getTimeInMillis(new Date());
//			log.debug("pollFreq: " + pollFreq);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		log.debug("<-- doQueryRegistration");
	}

	public long doQueryRegistration(String url) {
		setTarget(url);
		doQueryRegistration();
		return this.getPollFreq();
	}

	// public OadrCreatedPartyRegistrationType doCreatePartyRegistration() {
	public OadrPayload doCreatePartyRegistration() {
		log.debug("--> doCreatePartyRegistration");
		// crea un oggetto OadrCreatePartyRegistrationType
		// imposta i campi di OadrCreatePartyRegistrationType
		// invia l'oggetto OadrCreatePartyRegistrationType
		// riceve in risposta l'oggetto OadrCreatedPartyRegistrationType
		// lo processa

		OadrCreatePartyRegistrationType oCrPaRe = new OadrCreatePartyRegistrationType();
		oCrPaRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
		oCrPaRe.setOadrProfileName(SCHEMA_VERSION);
		oCrPaRe.setOadrTransportName(OadrTransportType.SIMPLE_HTTP);
		oCrPaRe.setOadrReportOnly(false);
		oCrPaRe.setOadrXmlSignature(false);
		oCrPaRe.setOadrVenName(VEN_NAME);
		oCrPaRe.setVenID(VEN_ID);
		oCrPaRe.setOadrHttpPullModel(new Boolean(VEN_PULL_MODEL));
		oCrPaRe.setOadrTransportAddress(VEN_TRANSPORT_ADDRESS);
		// OadrCreatedPartyRegistrationType oCredPaRe2 = null;
		OadrPayload oCredPaRe2 = null;
//		if (REGISTRATION != null) {
//			oCrPaRe.setRegistrationID(REGISTRATION.getRegistrationID());
//		}
		try {
			log.debug("oCrPaRe: " + oCrPaRe);
			oCredPaRe2 = sendOadrCreatePartyRegistration(oCrPaRe);
//			REGISTRATION = oCredPaRe2;
			log.debug("oCredPaRe2: " + oCredPaRe2);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		log.debug("<-- doCreatePartyRegistration");
		return oCredPaRe2;
	}

	public String doCreatePartyRegistration(RegistrationInfo info) {
		this.setRegistrationInfo(info);
		this.doCreatePartyRegistration();
		String registrationId = this.getRegistrationID();
		return registrationId;
	}

	public void doCancelPartyRegistration() {
		// cancella la registrazione del ven sul vtn
		if (REGISTRATION != null) {
			OadrCancelPartyRegistrationType oCaPaRe = new OadrCancelPartyRegistrationType();
			oCaPaRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
			oCaPaRe.setVenID(VEN_ID);
			oCaPaRe.setSchemaVersion(SCHEMA_VERSION);
			oCaPaRe.setRegistrationID(REGISTRATION.getRegistrationID());
			try {
				// OadrCanceledPartyRegistrationType oCaedPaRe =
				// sendOadrCancelPartyRegistration(oCaPaRe);
				OadrPayload oCaedPaRe = sendOadrCancelPartyRegistration(oCaPaRe);
				// REGISTRATION = null;
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
		}
	}

	public void doCancelReport() {
		// cancella tutti i report registrati
		if (!LIST_REPORT_REQUEST_ID.isEmpty()) {
			// if (LIST_REPORT_REQUEST_ID != null) {
//        		OadrCancelReportType oCaRe = new OadrCancelReportType();
//        		oCaRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
//        		oCaRe.setVenID(VEN_ID);
//        		oCaRe.setSchemaVersion(SCHEMA_VERSION);
//        		oCaRe.getReportRequestID().addAll(LIST_REPORT_REQUEST_ID);
//        		oCaRe.setReportToFollow(false);
//        		try {
//        			OadrCanceledReportType oCaedRe = sendOadrCancelReport(oCaRe);
//        		} catch (Exception ex) {
//        			log.error(ex.getMessage(), ex);
//        		}
			// LIST_REPORT_REQUEST_ID = null;
			LIST_REPORT_REQUEST_ID.clear();
			mapQueueOadrCreateReport = null;
			mapMapOadrCreateReport = null;
		}
	}

	public OadrReportDescriptionType makeReportDescription(String marketContext, String rID, String resourceID,
			JAXBElement itemBase, String reportType, ReadingTypeEnumeratedType readingType, int minPeriod,
			int maxPeriod, DurationModifier durationModifier, boolean onChange) {
		OadrReportDescriptionType reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID(rID);
		EiTargetType target = new EiTargetType();
		target.getResourceID().add(resourceID);
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType(reportType);
		reportDescription.setReadingType(readingType.value());
		reportDescription.setMarketContext(marketContext);

		OadrSamplingRateType samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT" + minPeriod + durationModifier.getValue());
		samplingRate.setOadrMaxPeriod("PT" + maxPeriod + durationModifier.getValue());
		samplingRate.setOadrOnChange(onChange);
		reportDescription.setOadrSamplingRate(samplingRate);

		if (itemBase != null)
			reportDescription.setItemBase(itemBase);

		return reportDescription;
	}

	public OadrReportType makeMetadataTelemetryStatus(String marketContext) {
		OadrReportType metadataTelemetryStatus = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataTelemetryStatus.setDuration(myDurationPropType);

		OadrReportDescriptionType reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID("resource1_status");
		EiTargetType target = new EiTargetType();
		target.getResourceID().add("resource1");
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType("x-resourceStatus");
		reportDescription.setReadingType("x-notApplicable");
		reportDescription.setMarketContext(marketContext);

		OadrSamplingRateType samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT1M");
		samplingRate.setOadrMaxPeriod("PT1M");
		samplingRate.setOadrOnChange(false);
		reportDescription.setOadrSamplingRate(samplingRate);

		metadataTelemetryStatus.getOadrReportDescription().add(reportDescription);

		metadataTelemetryStatus.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_telemetry_status";
		metadataTelemetryStatus.setReportSpecifierID(reportSpecifierID);
		metadataTelemetryStatus.setReportName("METADATA_TELEMETRY_STATUS");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataTelemetryStatus.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return metadataTelemetryStatus;
	}

	public OadrReportType makeMetadataTelemetryStatus(String marketContext, List<Resource> resources) {
		OadrReportType metadataTelemetryStatus = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataTelemetryStatus.setDuration(myDurationPropType);

		for (Resource r : resources) {
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext, r.getName() + "_status",
					r.getName(), null, ReportEnumeratedType.X_RESOURCE_STATUS.value(),
					ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES, false);
			metadataTelemetryStatus.getOadrReportDescription().add(reportDescription);
		}

		metadataTelemetryStatus.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_telemetry_status";
		metadataTelemetryStatus.setReportSpecifierID(reportSpecifierID);
		metadataTelemetryStatus.setReportName("METADATA_TELEMETRY_STATUS");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataTelemetryStatus.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return metadataTelemetryStatus;
	}

	public OadrReportType makeMetadataTelemetryUsage(String marketContext) {
		OadrReportType metadataTelemetryUsage = new OadrReportType();
		// imposta il report METADATA_TELEMETRY_USAGE

		// metadataTelemetryUsage.getDuration().setDuration("PT2H");

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataTelemetryUsage.setDuration(myDurationPropType);

		OadrReportDescriptionType reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID("resource1_energy");
		EiTargetType target = new EiTargetType();
		target.getResourceID().add("resource1");
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType("usage");

		EnergyRealType energyReal = new EnergyRealType();
		energyReal.setItemDescription("RealEnergy");
		energyReal.setItemUnits("Wh");
		energyReal.setSiScaleCode("n");

//        JAXBElement<EnergyRealType> jaxbEnergyReal=  new JAXBElement( 
//                new QName("energyReal"), EnergyRealType.class, energyReal);
		JAXBElement jaxbEnergyReal = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
				.createEnergyReal(energyReal);

		reportDescription.setItemBase(jaxbEnergyReal);
		reportDescription.setReadingType("Direct Read");
		reportDescription.setMarketContext(marketContext);

		OadrSamplingRateType samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT1M");
		samplingRate.setOadrMaxPeriod("PT1M");
		samplingRate.setOadrOnChange(false);
		reportDescription.setOadrSamplingRate(samplingRate);

		metadataTelemetryUsage.getOadrReportDescription().add(reportDescription);

		reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID("resource1_power");
		target = new EiTargetType();
		target.getResourceID().add("resource1");
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType("usage");

		PowerRealType powerReal = new PowerRealType();
		powerReal.setItemDescription("RealPower");
		powerReal.setItemUnits("W");
		powerReal.setSiScaleCode("n");

		PowerAttributesType powerAttributes = new PowerAttributesType();
		powerAttributes.setAc(false);
		powerAttributes.setHertz(new BigDecimal(60));
		powerAttributes.setVoltage(new BigDecimal(110));
		powerReal.setPowerAttributes(powerAttributes);

//        JAXBElement<PowerRealType> jaxbPowerReal=  new JAXBElement( 
//                new QName("powerReal"), PowerRealType.class, powerReal);
		JAXBElement jaxbPowerReal = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
				.createPowerReal(powerReal);

		reportDescription.setItemBase(jaxbPowerReal);
		reportDescription.setReadingType("Direct Read");
		reportDescription.setMarketContext(marketContext);

		samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT1M");
		samplingRate.setOadrMaxPeriod("PT1M");
		samplingRate.setOadrOnChange(false);
		reportDescription.setOadrSamplingRate(samplingRate);

		metadataTelemetryUsage.getOadrReportDescription().add(reportDescription);

		metadataTelemetryUsage.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_telemetry_usage";
		metadataTelemetryUsage.setReportSpecifierID(reportSpecifierID);
		metadataTelemetryUsage.setReportName("METADATA_TELEMETRY_USAGE");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataTelemetryUsage.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return metadataTelemetryUsage;
	}

	public OadrReportType makeMetadataTelemetryUsage(String marketContext, List<Resource> resources) {
		OadrReportType metadataTelemetryUsage = new OadrReportType();
		// imposta il report METADATA_TELEMETRY_USAGE

		// metadataTelemetryUsage.getDuration().setDuration("PT2H");

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataTelemetryUsage.setDuration(myDurationPropType);

		for (Resource r : resources) {
			EnergyRealType energyReal = new EnergyRealType();
			energyReal.setItemDescription("RealEnergy");
			energyReal.setItemUnits("Wh");
			energyReal.setSiScaleCode("n");
			JAXBElement jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createEnergyReal(energyReal);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext, r.getName() + "_energy",
					r.getName(), jaxb, ReportEnumeratedType.USAGE.value(), ReadingTypeEnumeratedType.DIRECT_READ, 1, 1,
					DurationModifier.MINUTES, false);
			metadataTelemetryUsage.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("RealPower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			PowerAttributesType powerAttributes = new PowerAttributesType();
			powerAttributes.setAc(false);
			powerAttributes.setHertz(new BigDecimal(60));
			powerAttributes.setVoltage(new BigDecimal(110));
			powerReal.setPowerAttributes(powerAttributes);
			jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_power", r.getName(), jaxb,
					ReportEnumeratedType.USAGE.value(), ReadingTypeEnumeratedType.DIRECT_READ, 1, 1,
					DurationModifier.MINUTES, false);
			metadataTelemetryUsage.getOadrReportDescription().add(reportDescription);
		}

		metadataTelemetryUsage.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_telemetry_usage";
		metadataTelemetryUsage.setReportSpecifierID(reportSpecifierID);
		metadataTelemetryUsage.setReportName("METADATA_TELEMETRY_USAGE");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataTelemetryUsage.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return metadataTelemetryUsage;
	}

	public OadrReportType makeMetadataHistoryUsage(String marketContext) {

		// imposta il report METADATA_HISTORY_USAGE
		OadrReportType metadataHistoryUsage = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataHistoryUsage.setDuration(myDurationPropType);

		// metadataHistoryUsage.getDuration().setDuration("PT2H");

		OadrReportDescriptionType reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID("resource1_energy");
		EiTargetType target = new EiTargetType();
		target.getResourceID().add("resource1");
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType("usage");

		EnergyRealType energyReal = new EnergyRealType();
		energyReal.setItemDescription("RealEnergy");
		energyReal.setItemUnits("Wh");
		energyReal.setSiScaleCode("n");

//        JAXBElement jaxbEnergyReal=  new JAXBElement( 
//                new QName("energyReal"), EnergyRealType.class, energyReal);
		JAXBElement jaxbEnergyReal = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
				.createEnergyReal(energyReal);

		reportDescription.setItemBase(jaxbEnergyReal);
		reportDescription.setReadingType("Direct Read");
		reportDescription.setMarketContext(marketContext);

		OadrSamplingRateType samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT1M");
		samplingRate.setOadrMaxPeriod("PT1M");
		samplingRate.setOadrOnChange(false);
		reportDescription.setOadrSamplingRate(samplingRate);

		metadataHistoryUsage.getOadrReportDescription().add(reportDescription);

		reportDescription = new OadrReportDescriptionType();

		reportDescription.setRID("resource1_power");
		target = new EiTargetType();
		target.getResourceID().add("resource1");
		reportDescription.setReportDataSource(target);
		reportDescription.setReportType("usage");

		PowerRealType powerReal = new PowerRealType();
		powerReal.setItemDescription("RealPower");
		powerReal.setItemUnits("W");
		powerReal.setSiScaleCode("n");

		PowerAttributesType powerAttributes = new PowerAttributesType();
		powerAttributes.setAc(false);
		powerAttributes.setHertz(new BigDecimal(60));
		powerAttributes.setVoltage(new BigDecimal(110));
		powerReal.setPowerAttributes(powerAttributes);

//        JAXBElement jaxbPowerReal=  new JAXBElement( 
//                new QName("powerReal"), PowerRealType.class, powerReal);
		JAXBElement jaxbPowerReal = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
				.createPowerReal(powerReal);

		reportDescription.setItemBase(jaxbPowerReal);
		reportDescription.setReadingType("Direct Read");
		reportDescription.setMarketContext(marketContext);

		samplingRate = new OadrSamplingRateType();
		samplingRate.setOadrMinPeriod("PT1M");
		samplingRate.setOadrMaxPeriod("PT1M");
		samplingRate.setOadrOnChange(false);
		reportDescription.setOadrSamplingRate(samplingRate);

		metadataHistoryUsage.getOadrReportDescription().add(reportDescription);

		metadataHistoryUsage.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_history_usage";
		metadataHistoryUsage.setReportSpecifierID(reportSpecifierID);
		metadataHistoryUsage.setReportName("METADATA_HISTORY_USAGE");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataHistoryUsage.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return metadataHistoryUsage;
	}

	public OadrReportType makeMetadataHistoryUsage(String marketContext, List<Resource> resource) {

		// imposta il report METADATA_HISTORY_USAGE
		OadrReportType metadataHistoryUsage = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2H");
		metadataHistoryUsage.setDuration(myDurationPropType);

		// metadataHistoryUsage.getDuration().setDuration("PT2H");

		for (Resource r : resource) {
			EnergyRealType energyReal = new EnergyRealType();
			energyReal.setItemDescription("RealEnergy");
			energyReal.setItemUnits("Wh");
			energyReal.setSiScaleCode("n");
			JAXBElement jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createEnergyReal(energyReal);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext, r.getName() + "_energy",
					r.getName(), jaxb, ReportEnumeratedType.USAGE.value(), ReadingTypeEnumeratedType.DIRECT_READ, 1, 1,
					DurationModifier.MINUTES, false);
			metadataHistoryUsage.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("RealPower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			PowerAttributesType powerAttributes = new PowerAttributesType();
			powerAttributes.setAc(false);
			powerAttributes.setHertz(new BigDecimal(60));
			powerAttributes.setVoltage(new BigDecimal(110));
			powerReal.setPowerAttributes(powerAttributes);
			jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_power", r.getName(), jaxb,
					ReportEnumeratedType.USAGE.value(), ReadingTypeEnumeratedType.DIRECT_READ, 1, 1,
					DurationModifier.MINUTES, false);
			metadataHistoryUsage.getOadrReportDescription().add(reportDescription);
		}

		metadataHistoryUsage.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_history_usage";
		metadataHistoryUsage.setReportSpecifierID(reportSpecifierID);
		metadataHistoryUsage.setReportName("METADATA_HISTORY_USAGE");
		// LIST_REPORT_SPECIFIER_ID.add(reportSpecifierID);
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataHistoryUsage.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return metadataHistoryUsage;
	}

	public OadrReportType makeMetadataResourceCapacity(String marketContext, List<StorageResource> storageResources) {
		OadrReportType metadataResourceCapacity = new OadrReportType();
		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2" + DurationModifier.HOURS.getValue());
		metadataResourceCapacity.setDuration(myDurationPropType);

		for (StorageResource s : storageResources) {
			EnergyRealType energyReal = new EnergyRealType();
			energyReal.setItemDescription("RealEnergy");
			energyReal.setItemUnits("Wh");
			energyReal.setSiScaleCode("n");
			JAXBElement jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createEnergyReal(energyReal);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					s.getName() + "_battery_storage_capacity", s.getName(), jaxb, "x-resourceCapacity",
					ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES, false);
			metadataResourceCapacity.getOadrReportDescription().add(reportDescription);

			jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createEnergyReal(energyReal);
			reportDescription = makeReportDescription(marketContext, s.getName() + "_state_of_charge", s.getName(),
					jaxb, "x-resourceCapacity", ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES,
					false);
			metadataResourceCapacity.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("RealPower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			PowerAttributesType powerAttributes = new PowerAttributesType();
			powerAttributes.setAc(false);
			powerAttributes.setHertz(new BigDecimal(60));
			powerAttributes.setVoltage(new BigDecimal(110));
			powerReal.setPowerAttributes(powerAttributes);
			jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, s.getName() + "_maximum_input_power", s.getName(),
					jaxb, "x-resourceCapacity", ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES,
					false);
			metadataResourceCapacity.getOadrReportDescription().add(reportDescription);

			reportDescription = makeReportDescription(marketContext, s.getName() + "_maximum_output_power", s.getName(),
					jaxb, "x-resourceCapacity", ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES,
					false);
			metadataResourceCapacity.getOadrReportDescription().add(reportDescription);
		}

		metadataResourceCapacity.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_capacity";
		metadataResourceCapacity.setReportSpecifierID(reportSpecifierID);
		metadataResourceCapacity.setReportName("METADATA_x-RESOURCE_CAPACITY");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataResourceCapacity.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataResourceCapacity;
	}

	public OadrReportType makeMetadataActualFlexibility(String marketContext, List<Resource> resource) {
		OadrReportType metadataActualFlexibility = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT1" + DurationModifier.HOURS.getValue());
		metadataActualFlexibility.setDuration(myDurationPropType);

		for (Resource r : resource) {

			BaseUnitType baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("FlexibilityProvider");
			baseUnitType.setItemUnits("0=Plant,1=Technology");
			baseUnitType.setSiScaleCode("none");

			JAXBElement jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			// QName qName = new QName("oadr:customUnit");
			// JAXBElement<BaseUnitType> jaxb = new JAXBElement<BaseUnitType>(qName,
			// BaseUnitType.class, baseUnitType);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					r.getName() + "_flexibility_provider", r.getName(), jaxb, "x-actualFlexibility",
					ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES, false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("ServiceType");
			baseUnitType.setItemUnits("0=Provision,1=Absorption,2=SymmetricService");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_service_type", r.getName(), jaxb,
					"x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("AvailablePower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("k");
			JAXBElement jaxb2 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_available_power", r.getName(),
					jaxb2, "x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			EnergyRealType energyReal = new EnergyRealType();
			energyReal.setItemDescription("AvailableEnergy");
			energyReal.setItemUnits("Wh");
			energyReal.setSiScaleCode("k");
			JAXBElement jaxb3 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createEnergyReal(energyReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_available_energy", r.getName(),
					jaxb3, "x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("RampRate");
			baseUnitType.setItemUnits("MW/min");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_ramp_rate", r.getName(), jaxb,
					"x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("RampDuration");
			baseUnitType.setItemUnits("min");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_ramp_duration", r.getName(), jaxb,
					"x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("StartTime");
			baseUnitType.setItemUnits("datetime");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_start_time", r.getName(), jaxb,
					"x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("EndTime");
			baseUnitType.setItemUnits("datetime");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_end_time", r.getName(), jaxb,
					"x-actualFlexibility", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataActualFlexibility.getOadrReportDescription().add(reportDescription);

		}

		metadataActualFlexibility.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_actual_flexibility";
		metadataActualFlexibility.setReportSpecifierID(reportSpecifierID);
		metadataActualFlexibility.setReportName("METADATA_x-ACTUAL_FLEXIBILITY");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataActualFlexibility.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataActualFlexibility;
	}

	public OadrReportType makeMetadataFlexibilityOffered(String marketContext, List<Resource> resource) {
		OadrReportType metadataFlexibilityOffered = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2" + DurationModifier.HOURS.getValue());
		metadataFlexibilityOffered.setDuration(myDurationPropType);

		for (Resource r : resource) {

			BaseUnitType baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("FlexibilityPrice");
			baseUnitType.setItemUnits("Euro");
			baseUnitType.setSiScaleCode("none");
			// QName qName = new QName("oadr:currencyPerKWh");
			// JAXBElement<BaseUnitType> jaxb = new JAXBElement<BaseUnitType>(qName,
			// BaseUnitType.class, baseUnitType);
			JAXBElement jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					r.getName() + "_flexibility_price", r.getName(), jaxb, "price",
					ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES, false);
			metadataFlexibilityOffered.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("FlexibilityOffered");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			JAXBElement jaxb2 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_flexibility_offered", r.getName(),
					jaxb2, "x-flexibility_offered", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataFlexibilityOffered.getOadrReportDescription().add(reportDescription);

			powerReal = new PowerRealType();
			powerReal.setItemDescription("FlexibilityAccepted");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			jaxb2 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_flexibility_accepted", r.getName(),
					jaxb2, "x-flexibility_accepted", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataFlexibilityOffered.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("ReferenceBid");
			baseUnitType.setItemUnits("none");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_reference_bid", r.getName(), jaxb,
					"x-reference_bid", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataFlexibilityOffered.getOadrReportDescription().add(reportDescription);

		}

		metadataFlexibilityOffered.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_flexibility_offered";
		metadataFlexibilityOffered.setReportSpecifierID(reportSpecifierID);
		metadataFlexibilityOffered.setReportName("METADATA_x-FLEXIBILITY_OFFERED");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataFlexibilityOffered.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataFlexibilityOffered;
	}

	public OadrReportType makeMetadataBidResult(String marketContext, List<Resource> resource) {
		OadrReportType metadataBidResult = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2" + DurationModifier.HOURS.getValue());
		metadataBidResult.setDuration(myDurationPropType);

		for (Resource r : resource) {

			BaseUnitType baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("FlexibilityPrice");
			baseUnitType.setItemUnits("Euro");
			baseUnitType.setSiScaleCode("none");
			// QName qName = new QName("oadr:currencyPerKWh");
			// JAXBElement<BaseUnitType> jaxb = new JAXBElement<BaseUnitType>(qName,
			// BaseUnitType.class, baseUnitType);
			JAXBElement jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					r.getName() + "_flexibility_price", r.getName(), jaxb, "price",
					ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES, false);
			metadataBidResult.getOadrReportDescription().add(reportDescription);

			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("FlexibilityOffered");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			JAXBElement jaxb2 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_flexibility_offered", r.getName(),
					jaxb2, "x-flexibility_offered", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataBidResult.getOadrReportDescription().add(reportDescription);

			powerReal = new PowerRealType();
			powerReal.setItemDescription("FlexibilityAccepted");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			jaxb2 = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory().createPowerReal(powerReal);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_flexibility_accepted", r.getName(),
					jaxb2, "x-flexibility_accepted", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1,
					DurationModifier.MINUTES, false);
			metadataBidResult.getOadrReportDescription().add(reportDescription);

			baseUnitType = new BaseUnitType();
			baseUnitType.setItemDescription("ReferenceBid");
			baseUnitType.setItemUnits("none");
			baseUnitType.setSiScaleCode("none");
			// qName = new QName("oadr:customUnit");
			// jaxb = new JAXBElement<BaseUnitType>(qName, BaseUnitType.class,
			// baseUnitType);
			jaxb = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createCustomUnit(baseUnitType);
			reportDescription = makeReportDescription(marketContext, r.getName() + "_reference_bid", r.getName(), jaxb,
					"x-reference_bid", ReadingTypeEnumeratedType.X_NOT_APPLICABLE, 1, 1, DurationModifier.MINUTES,
					false);
			metadataBidResult.getOadrReportDescription().add(reportDescription);

		}

		metadataBidResult.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_bid_result";
		metadataBidResult.setReportSpecifierID(reportSpecifierID);
		metadataBidResult.setReportName("METADATA_x-BID_RESULT");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataBidResult.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataBidResult;
	}

	public OadrReportType makeMetadataResourceForecast(String marketContext, List<Resource> resource) {
		OadrReportType metadataResourceForecast = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2" + DurationModifier.HOURS.getValue());
		metadataResourceForecast.setDuration(myDurationPropType);

		for (Resource r : resource) {
			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("RealPower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			PowerAttributesType powerAttributes = new PowerAttributesType();
			powerAttributes.setAc(false);
			powerAttributes.setHertz(new BigDecimal(60));
			powerAttributes.setVoltage(new BigDecimal(110));
			powerReal.setPowerAttributes(powerAttributes);
			JAXBElement jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createPowerReal(powerReal);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					r.getName() + "_forecast", r.getName(), jaxb, "x-resourceCapacity",
					ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES, false);
			metadataResourceForecast.getOadrReportDescription().add(reportDescription);

			reportDescription = makeReportDescription(marketContext, r.getName() + "_max_flex", r.getName(), jaxb,
					"x-resourceCapacity", ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES, false);
			metadataResourceForecast.getOadrReportDescription().add(reportDescription);

			reportDescription = makeReportDescription(marketContext, r.getName() + "_min_flex", r.getName(), jaxb,
					"x-resourceCapacity", ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES, false);
			metadataResourceForecast.getOadrReportDescription().add(reportDescription);
		}

		metadataResourceForecast.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_forecast";
		metadataResourceForecast.setReportSpecifierID(reportSpecifierID);
		metadataResourceForecast.setReportName("METADATA_x-DEMAND_FORECAST");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataResourceForecast.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataResourceForecast;
	}

	public OadrReportType makeMetadataOptimizedProfile(String marketContext, List<Resource> resource) {
		OadrReportType metadataOptimizedProfile = new OadrReportType();

		DurationPropType myDurationPropType = new DurationPropType();
		myDurationPropType.setDuration("PT2" + DurationModifier.HOURS.getValue());
		metadataOptimizedProfile.setDuration(myDurationPropType);

		for (Resource r : resource) {
			PowerRealType powerReal = new PowerRealType();
			powerReal.setItemDescription("RealPower");
			powerReal.setItemUnits("W");
			powerReal.setSiScaleCode("n");
			PowerAttributesType powerAttributes = new PowerAttributesType();
			powerAttributes.setAc(false);
			powerAttributes.setHertz(new BigDecimal(60));
			powerAttributes.setVoltage(new BigDecimal(110));
			powerReal.setPowerAttributes(powerAttributes);
			JAXBElement jaxb = new org.oasis_open.docs.ns.emix._2011._06.power.ObjectFactory()
					.createPowerReal(powerReal);
			OadrReportDescriptionType reportDescription = makeReportDescription(marketContext,
					r.getName() + "_optimized_profile", r.getName(), jaxb, "x-optimizedProfile",
					ReadingTypeEnumeratedType.DIRECT_READ, 1, 1, DurationModifier.MINUTES, false);
			metadataOptimizedProfile.getOadrReportDescription().add(reportDescription);
		}

		metadataOptimizedProfile.setReportRequestID("0");
		String reportSpecifierID = generateID(NUM_DIGIT_REQUEST_ID) + "_optimized_profile";
		metadataOptimizedProfile.setReportSpecifierID(reportSpecifierID);
		metadataOptimizedProfile.setReportName("METADATA_x-OPTIMIZED_PROFILE");
		mapReportSpecificerMarketContext.put(reportSpecifierID, marketContext);

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			metadataOptimizedProfile.setCreatedDateTime(dateNow);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return metadataOptimizedProfile;
	}

	public void sendCapacityReport(OadrRegisteredReportType in) throws Exception {
		OadrResponseType response = null;
		OadrReportRequestType request = in.getOadrReportRequest().get(0);
		// controllo che esista lo specifier id inviato
		if (mapReportSpecificerMarketContext.get(request.getReportSpecifier().getReportSpecifierID()) != null
				&& request.getReportSpecifier().getReportSpecifierID().endsWith("capacity")) {
			EiResponseType eiResponse = new EiResponseType();
			eiResponse.setResponseCode("200");
			eiResponse.setResponseDescription("OK");
			eiResponse.setRequestID(in.getEiResponse().getRequestID());
			OadrCreatedReportType ret = new OadrCreatedReportType();
			ret.setEiResponse(eiResponse);
			ret.setSchemaVersion(SCHEMA_VERSION);
			ret.setVenID(VEN_ID);
			String myReportRequestID = request.getReportRequestID();
			OadrPendingReportsType myOadrPendingReportsType = new OadrPendingReportsType();
			myOadrPendingReportsType.getReportRequestID().add(myReportRequestID);
			ret.setOadrPendingReports(myOadrPendingReportsType);
			OadrResponseType res = sendOadrCreatedReport(ret);
			if (res.getEiResponse().getResponseCode().equals("200")) {
				// mando report con i dati
				Dao<StorageResource> daoStorage = new Dao<>(StorageResource.class);
				List<StorageResource> storageResources = daoStorage.findByField("resourceType", "storage");
				if (!storageResources.isEmpty()) {
					OadrUpdateReportType ourt = new OadrUpdateReportType();
					ourt.setRequestID(generateID(10));

					OadrReportType myOadrReportType = new OadrReportType();
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
					Dtstart myDtstart = null;
					DatatypeFactory datatypeFactory = null;
					XMLGregorianCalendar dateNow = null;

					try {
						datatypeFactory = DatatypeFactory.newInstance();
						dateNow = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
						myOadrReportType.setCreatedDateTime(dateNow);

						myDtstart = new Dtstart();
						myDtstart.setDateTime(dateNow);
						myOadrReportType.setDtstart(myDtstart);
					} catch (DatatypeConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Intervals myIntervals = new Intervals();
					IntervalType myIntervalType = new IntervalType();
					myIntervalType.setDtstart(myDtstart);
					myIntervalType.setDuration(request.getReportSpecifier().getReportBackDuration());
					for (StorageResource storageResource : storageResources) {
						JAXBElement jaxbReportPayloadType = createReportPayload(
								storageResource.getName() + "_battery_storage_capacity",
								storageResource.getMaxCapacity());
						myIntervalType.getStreamPayloadBase().add(jaxbReportPayloadType);

						jaxbReportPayloadType = createReportPayload(storageResource.getName() + "_maximum_input_power",
								storageResource.getChargeRate());
						myIntervalType.getStreamPayloadBase().add(jaxbReportPayloadType);

						jaxbReportPayloadType = createReportPayload(storageResource.getName() + "_maximum_output_power",
								storageResource.getDischargeRate());
						myIntervalType.getStreamPayloadBase().add(jaxbReportPayloadType);
					}
					myIntervals.getInterval().add(myIntervalType);
					myOadrReportType.setIntervals(myIntervals);
					myOadrReportType.setReportRequestID(myReportRequestID);
					myOadrReportType.setReportSpecifierID(request.getReportSpecifier().getReportSpecifierID());
					myOadrReportType.setReportName("x-RESOURCE_CAPACITY");
					myOadrReportType.setCreatedDateTime(dateNow);
					ourt.setOadrReport(myOadrReportType);
					ourt.setVenID(VEN_ID);
					sendOadrUpdateReport(ourt);
				}
			}
		}
	}

	private JAXBElement createReportPayload(String name, float value) {
		OadrReportPayloadType myOadrReportPayloadType = new OadrReportPayloadType();
		myOadrReportPayloadType.setRID(name);
		PayloadFloatType myPayloadFloatType = new PayloadFloatType();
		myPayloadFloatType.setValue(value);
		JAXBElement jaxbPayloadFloatType = new org.oasis_open.docs.ns.energyinterop._201110.ObjectFactory()
				.createPayloadFloat(myPayloadFloatType);
		myOadrReportPayloadType.setPayloadBase(jaxbPayloadFloatType);
		myOadrReportPayloadType.setOadrDataQuality("Quality Good - Non Specific");
		JAXBElement jaxbReportPayloadType = new org.openadr.oadr_2_0b._2012._07.ObjectFactory()
				.createOadrReportPayload(myOadrReportPayloadType);
		return jaxbReportPayloadType;
	}

	public void doRegisterReport() {
		// crea un oggetto OadrRegisterReportType
		// imposta i campi di OadrRegisterReportType
		// invia l'oggetto OadrRegisterReportType
		// riceve in risposta l'oggetto OadrRegisteredReportType
		// lo processa
		OadrRegisterReportType oReRe = new OadrRegisterReportType();
		oReRe.setSchemaVersion(SCHEMA_VERSION);
		oReRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
		oReRe.setVenID(VEN_ID);

		// per ogni item di LIST_MARKET_CONTEXT:
		for (Iterator<String> iterator = LIST_MARKET_CONTEXT.iterator(); iterator.hasNext();) {
			String myMarketContext = iterator.next();
			// crea metadataTelemetryStatus
			oReRe.getOadrReport().add(makeMetadataTelemetryStatus(myMarketContext));
			// crea metadataTelemetryUsage
			oReRe.getOadrReport().add(makeMetadataTelemetryUsage(myMarketContext));
			// crea metadataHistoryUsage
			oReRe.getOadrReport().add(makeMetadataHistoryUsage(myMarketContext));
		}
		log.debug("oReRe: " + oReRe);
		try {
			// OadrRegisteredReportType oReedRe = sendOadrRegisterReport(oReRe);
			OadrPayload oReedRe = sendOadrRegisterReport(oReRe);
			log.debug("oReedRe: " + oReedRe);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
	}

	// risorse prese dal database
	// public OadrRegisteredReportType registerReportCustomResources() {
	public OadrPayload registerReportCustomResources() {

		log.debug("--> registerReportCustomResources");
		// crea un oggetto OadrRegisterReportType
		// imposta i campi di OadrRegisterReportType
		// invia l'oggetto OadrRegisterReportType
		// riceve in risposta l'oggetto OadrRegisteredReportType
		// lo processa
		OadrRegisterReportType oReRe = new OadrRegisterReportType();
		oReRe.setSchemaVersion(SCHEMA_VERSION);
		oReRe.setRequestID(generateID(NUM_DIGIT_REQUEST_ID));
		oReRe.setVenID(VEN_ID);

		// per ogni item di LIST_MARKET_CONTEXT:
		for (Iterator<String> iterator = LIST_MARKET_CONTEXT.iterator(); iterator.hasNext();) {
			String myMarketContext = iterator.next();
			Dao<Resource> dao = new Dao<>(Resource.class);
			List<Resource> resources = dao.getAll(null, null);

			Dao<StorageResource> daoStorage = new Dao<>(StorageResource.class);
			List<StorageResource> storageResources = daoStorage.findByField("resourceType", "storage");
			oReRe.getOadrReport().add(makeMetadataTelemetryStatus(myMarketContext, resources));
			oReRe.getOadrReport().add(makeMetadataTelemetryUsage(myMarketContext, resources));
			oReRe.getOadrReport().add(makeMetadataHistoryUsage(myMarketContext, resources));
			oReRe.getOadrReport().add(makeMetadataActualFlexibility(myMarketContext, resources));
			oReRe.getOadrReport().add(makeMetadataFlexibilityOffered(myMarketContext, resources));
//			oReRe.getOadrReport().add(makeMetadataBidResult(myMarketContext, resources));
//          oReRe.getOadrReport().add(makeMetadataResourceCapacity(myMarketContext, storageResources));
//          oReRe.getOadrReport().add(makeMetadataResourceForecast(myMarketContext, resources));
//          oReRe.getOadrReport().add(makeMetadataOptimizedProfile(myMarketContext, resources));
		}
		// OadrRegisteredReportType oReedRe = null;
		OadrPayload oReedRe = null;
		log.debug("oReRe: " + oReRe);
		try {
			oReedRe = sendOadrRegisterReport(oReRe);
//			if (oReedRe.getEiResponse().getResponseCode().equals("200"))
// ToDO				registeredReport = true;
//			if (oReedRe.getOadrReportRequest() != null) {
// ToDO				saveReportRequests(oReedRe.getOadrReportRequest());
//    			sendCapacityReport(oReedRe);
//      		sendActualFlexibilityReport(oReedRe);
// 		    	sendFlexibilityOfferedReport(oReedRe);
//			}
//
//			log.debug("oReedRe: " + oReedRe);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
		log.debug("<-- registerReportCustomResources");
		return oReedRe;
	}

	public Boolean doSetPoll() {
		log.debug("--> doSetPoll");

		Boolean resFirstPolling = true;
//		if (new Boolean(VEN_PULL_MODEL).booleanValue()) {
//			// attiva un loop di oadrPoll nel caso Oadr20bImpl.getInstance().VEN_PULL_MODEL
//			// sia true
//			RunOadrPollTask task = new RunOadrPollTask();
//
//			ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//			ScheduledFuture<Boolean> result = executor.schedule(task, 0, TimeUnit.SECONDS);
//			try {
//				resFirstPolling = result.get();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//				return false;
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//				return false;
//			}
//			if (resFirstPolling == true) {
//				PeriodicTask periodicTask = new PeriodicTask();
//				TIMER = new Timer();
//				TIMER.schedule(periodicTask, 0, pollFreq);
//				this.isPollRunning = true;
//			}
//		}

		if (new Boolean(VEN_PULL_MODEL).booleanValue()) {
			// attiva un loop di oadrPoll nel caso Oadr20bImpl.getInstance().VEN_PULL_MODEL
			// sia true
			TimerTask task = new RunOadrPollTask();
			TIMER = new Timer();
			log.debug("pollFreq: " + pollFreq);
			TIMER.schedule(task, 0, pollFreq);
			this.isPollRunning = true;
		}
		log.debug("<-- doSetPoll");
		return resFirstPolling;
	}

	public void doResetPoll() {

		if (new Boolean(VEN_PULL_MODEL).booleanValue()) {
			// cancella il timer per il polling
			if (TIMER != null) {
				TIMER.cancel();
				TIMER.purge();
			}
			this.isPollRunning = false;
		}
	}

	public String doResponse() {

		// ritorna una EiResponseType con HTTP code 200 OK
		String ret = null;
		EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType oadrResponse = new OadrResponseType();
		oadrResponse.setEiResponse(eiResponse);
		oadrResponse.setSchemaVersion(SCHEMA_VERSION);
		oadrResponse.setVenID(VEN_ID);
		try {
			// ret = Parser.marshal(out);
//    		QName qName = new QName("oadrResponse");
//    		JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory()
					.createOadrResponse(oadrResponse);

			ret = Parser.marshal(root);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public synchronized ReplyMessage startPolling(RegistrationInfo info) {
		log.debug("--> startPolling");
		setRegistrationInfo(info);
		ReplyMessage replyMessage = new ReplyMessage();
		log.debug("--> venIsRegistered:" + venIsRegistered());
		if (!venIsRegistered()) {
			// esegue una QueryRegistration
			this.doQueryRegistration();

			// esegue una CreatePartyRegistration
			// OadrCreatedPartyRegistrationType registrationRes =
			// this.doCreatePartyRegistration();
			OadrPayload registrationRes = this.doCreatePartyRegistration();

			// replyMessage.setResponseCode(registrationRes.getEiResponse().getResponseCode());
			// replyMessage.setResponseMessage(registrationRes.getEiResponse().getResponseDescription());
			replyMessage.setResponseCode("200");
			replyMessage.setResponseMessage("OK");

			// if(registrationRes.getEiResponse().getResponseCode().equals("200")){
			// esegue una RegisterReport
			// OadrRegisteredReportType reportRes = registerReportCustomResources();
			OadrPayload reportRes = registerReportCustomResources();
			// replyMessage.setResponseCode(reportRes.getEiResponse().getResponseCode());
			// replyMessage.setResponseMessage(reportRes.getEiResponse().getResponseDescription());
			replyMessage.setResponseCode("200");
			replyMessage.setResponseMessage("OK");

			// parte il polling
			// if(reportRes.getEiResponse().getResponseCode().equals("200")){
			Boolean res = this.doSetPoll();
			if (res == true) {
				replyMessage.setResponseCode("200");
				replyMessage.setResponseMessage("OK");
			} else {
				replyMessage.setResponseCode("500");
				replyMessage.setResponseMessage("Error! Polling not started.");
			}
			// }
			// }
		} else {
			replyMessage.setResponseCode("200");
			replyMessage.setResponseMessage("OK");
			this.doSetPoll();
		}

		log.debug("<-- startPolling");
		return replyMessage;
	}

	public boolean venIsRegistered() {
		if (REGISTRATION != null && registeredReport == true)
			return true;
		else
			return false;
	}

	public synchronized void stopPolling() {
		log.debug("stopPolling");

		this.doResetPoll();
	}

	public boolean togglePolling(RegistrationInfo info) throws InterruptedException, ExecutionException {
		log.debug("--> togglePolling");
		log.debug("Flag isPollRunning:" + this.isPollRunning);
		if (this.isPollRunning) {
			this.stopPolling();
		} else {
			this.startPolling(info);
		}
		log.debug("<-- togglePolling");
		return this.isPollRunning;
	};

	public synchronized boolean isPollRunning() {
		return this.isPollRunning;
	}

	public void cancelRegistration() throws Exception {
		if (REGISTRATION != null) {
			OadrCancelPartyRegistrationType oCaPaRe = new OadrCancelPartyRegistrationType();
			oCaPaRe.setRequestID("1234567890D");
			oCaPaRe.setVenID(VEN_ID);
			oCaPaRe.setRegistrationID(REGISTRATION.getRegistrationID());
			// OadrCanceledPartyRegistrationType oCaedPaRe =
			// sendOadrCancelPartyRegistration(oCaPaRe);
			OadrPayload oCaedPaRe = sendOadrCancelPartyRegistration(oCaPaRe);
			REGISTRATION = null;
		}
	}

	public String generateID(int count) {
		String ALPHA_NUMERIC_STRING = "abcdef0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		// genera un ID pseudo casuale composto da numeri e lettere
		return builder.toString();
	}
}
