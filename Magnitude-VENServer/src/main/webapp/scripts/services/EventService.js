(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('EventService', [
        '$resource', 'BASE_PATH',
        function ($resource, basePath) {
            var event = $resource(basePath + '/OpenADR2/events/:id',
                {
                    id: '@id'
                },
                {
                    queryAll: {
                        method: 'GET',
                        isArray: true
                    },
                    query: {
                        method: 'GET',
                        isArray: false
                    },
                    update: {
                        method: 'PUT'
                    }
                });
            return event;
        }
    ]);
}(this, angular));