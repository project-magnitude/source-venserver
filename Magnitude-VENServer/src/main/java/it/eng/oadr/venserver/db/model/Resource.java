package it.eng.oadr.venserver.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.Gson;

@Entity
@XmlRootElement
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Resource extends AbstractEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name="name")
	private String name;
	
	@Column(name="online")
	private Boolean online;
	
	@Column(name="w")
	private float w;
	
	@Column(name="resourceType")
	private String resourceType;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getOnline() {
		return online;
	}
	public void setOnline(Boolean online) {
		this.online = online;
	}
	public float getW() {
		return w;
	}
	public void setW(float w) {
		this.w = w;
	}
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	public static Resource resourceFactory(String resourceObj) {
		Gson gson = new Gson();
		Resource r = gson.fromJson(resourceObj, StorageResource.class);
		switch(r.getResourceType()){
			case "load":
				r = gson.fromJson(resourceObj, LoadResource.class);
				break;
			case "generator":
				r = gson.fromJson(resourceObj, GeneratorResource.class);
				break;
			case "storage":
				r = gson.fromJson(resourceObj, StorageResource.class);
				break;
		}
		return r;
	}
}
