(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('LogService', [
        '$resource',  'BASE_PATH',
        function ($resource, basePath) {
            var log = $resource(basePath + '/OpenADR2/log/:id',
                {
                    id: '@id'
                },
                {
                    queryAll: {
                        method: 'GET',
                        isArray: true
                    },
                    query: {
                        method: 'GET',
                        isArray: false
                    },
                    update: {
                        method: 'PUT'
                    },
                    isPollingRunning: {
                        method: 'GET',
                        url: basePath + '/OpenADR2/log/polling-state',
                        transformResponse: function (data, headers) {
                            return { data: data === 'true' };
                        }
                    }
                });
            return log;
        }
    ]);
}(this, angular));
