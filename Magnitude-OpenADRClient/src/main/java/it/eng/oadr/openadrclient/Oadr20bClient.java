package it.eng.oadr.openadrclient;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.openadrmodel.Parser;

/**
 *
 * @author marco
 */
public class Oadr20bClient {

    private static final Logger log = LoggerFactory.getLogger(Oadr20bClient.class);
    private static Client client = null;
    private static String target = null;
    private static java.util.Properties props = null;
    
    public enum PATH {
    	EI_REGISTER_PARTY("EiRegisterParty"),
    	EI_EVENT("EiEvent"),
    	EI_REPORT("EiReport"),
    	EI_OPT("EiOpt"),
    	OADR_POLL("OadrPoll");

    	private String path;
    	private PATH(String path) {
    		this.path = path;
    	}
    	@Override
    	public String toString() {
    		return this.path;
    	}
    };

    private Oadr20bClient() {
        SSLContext sslcontext = null;
//        String username = null;
//        String password = null;
        try {
            target = getProperty("oadr20bClient.target");
//            username = getProperty("oadr20bClient.username");
//            password = getProperty("oadr20bClient.password");
            sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

            }}, new java.security.SecureRandom());
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        client = ClientBuilder.newBuilder()
                /*
                .sslContext(sslcontext)
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                 */
                .build();
//        client.register(HttpAuthenticationFeature.basic(username, password));
    }

    private static Oadr20bClient INSTANCE = null;

    public synchronized static Oadr20bClient getInstance() throws JAXBException {
        if (INSTANCE == null) {
            INSTANCE = new Oadr20bClient();
        }
        return INSTANCE;
    }
    
    public void setTarget(String myTarget){
    	target = myTarget;
    }
    
    public String getTarget(){
    	return target;
    }
    

    private synchronized String getProperty(String key) throws IOException, Exception {
        String ret = null;
        if (props == null) {
            props = new java.util.Properties();
			final String ELSA_ENV = System.getenv("ELSA_ENV");
			String propertyName = "/oadr20bclient";
			if (ELSA_ENV != null && !ELSA_ENV.equals("")) {
				propertyName += "." + ELSA_ENV;
			}
			propertyName += ".properties";
			log.debug("propertyName: " + propertyName);
            props.load(Oadr20bClient.class.getResourceAsStream(propertyName));
        }
        ret = props.getProperty(key);
        if (ret == null) {
            throw new Exception("Property " + key + " not found");
        }
        return ret;
    }

    public OadrPayload postEiRegisterParty(OadrPayload oadrPayload) throws Exception {
        OadrPayload ret = null;
        log.debug("endpoint: " + target + "/EiRegisterParty");
        String input = Parser.marshal(oadrPayload);
        log.debug("input: " + input);
        String output = client.target(target)
                .path("EiRegisterParty")
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        return ret;
    }

    public OadrPayload postEiEvent(OadrPayload oadrPayload) throws Exception {
        OadrPayload ret = null;
        log.debug("endpoint: " + target + "/EiEvent");
        String input = Parser.marshal(oadrPayload);
        log.debug("input: " + input);
        String output = client.target(target)
                .path("EiEvent")
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        return ret;
    }

    public OadrPayload postEiReport(OadrPayload oadrPayload) throws Exception {
        OadrPayload ret = null;
        log.debug("endpoint: " + target + "/EiReport");
        String input = Parser.marshal(oadrPayload);
        log.debug("input: " + input);
        String output = client.target(target)
                .path("EiReport")
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        return ret;
    }

    public OadrPayload postEiOpt(OadrPayload oadrPayload) throws Exception {
        OadrPayload ret = null;
        log.debug("endpoint: " + target + "/EiOpt");
        String input = Parser.marshal(oadrPayload);
        log.debug("input: " + input);
        String output = client.target(target)
                .path("EiOpt")
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        return ret;
    }

    public OadrPayload postOadrPoll(OadrPayload oadrPayload) throws Exception {
        OadrPayload ret = null;
        //log.debug("endpoint: " + target + "/OadrPoll");
        String input = Parser.marshal(oadrPayload);
        //log.debug("input: " + input);
        String output = client.target(target)
                .path("OadrPoll")
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        //log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        if(ret.getOadrSignedObject().getOadrResponse() == null){
           log.debug("output: " + output);
        }
        return ret;
    }
    
    public OadrPayload post(PATH path, OadrPayload oadrPayload) throws Exception {
    	OadrPayload ret = null;
        log.debug("endpoint: " + target + "/" + path.toString());
        String input = Parser.marshal(oadrPayload);
        log.debug("input: " + input);
        String output = client.target(target)
                .path(path.toString())
                .request()
                .post(Entity.entity(input, MediaType.APPLICATION_XML_TYPE), String.class);
        log.debug("output: " + output);
        ret = (OadrPayload) Parser.unmarshal(output);
        return ret;
    }
}
