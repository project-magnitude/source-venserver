package it.eng.oadr.venserver.socket;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.corundumstudio.socketio.listener.ExceptionListener;

import io.netty.channel.ChannelHandlerContext;
import it.eng.oadr.venserver.impl.VenProperties;

public enum SocketServer {
	INSTANCE;
	
	private final Logger logger = LoggerFactory.getLogger(SocketServer.class);
	private final SocketIOServer server;

	public enum EVENT_NAME {
		POLLING("polling"),
		REPORT_REQUEST("report_request"),
		EVENT("event");
		
		private String eventName;
		private EVENT_NAME(String eventName) {
			this.eventName = eventName;
		}
		
		@Override
		public String toString() {
			return this.eventName;
		}
	}
	
	private SocketServer() {
		logger.debug("-->SocketServer");
		
//		SocketConfig socketConfig = new SocketConfig();
//		socketConfig.setReuseAddress(true);
		
		String venHostname = "mucho";
		int socketPort = 8084;
		try {
			venHostname = VenProperties.getProperty("venHostname");
			logger.info("SocketServer is running on hostname: " + venHostname);
			
			socketPort = Integer.valueOf(VenProperties.getProperty("socket.port"));
		} catch (Exception ex) {
			logger.error("Cannot read venHostname or socket.port, setting it to defaults value. The SocketServer could not be properly initalized");
			logger.error(ex.getMessage(), ex);
		}

		Configuration config = new Configuration();
//		config.setTransports(Transport.POLLING);
//		config.setHostname("192.168.30.194");
//		config.setHostname("localhost");
		config.setHostname(venHostname);
		config.setPort(socketPort);
//		config.setSocketConfig(socketConfig);
//		config.setOrigin("http://localhost:8080");

		config.setExceptionListener(new ExceptionListener() {
			
			@Override
			public void onEventException(Exception e, List<Object> args, SocketIOClient client) {
				logger.error(e.getMessage(), e);
			}
			
			@Override
			public void onDisconnectException(Exception e, SocketIOClient client) {
				logger.error(e.getMessage(), e);
			}
			
			@Override
			public void onConnectException(Exception e, SocketIOClient client) {
				logger.error(e.getMessage(), e);
			}
			
			@Override
			public boolean exceptionCaught(ChannelHandlerContext ctx, Throwable e) throws Exception {
				logger.error(e.getMessage(), e);
				return false;
			}
		});
		
		logger.debug("listening to port: " + config.getPort());

		server = new SocketIOServer(config);
		server.addConnectListener(new ConnectListener(){
			@Override
			public void onConnect(SocketIOClient arg0) {
				logger.debug("onConnect");
			}
		});
		server.addDisconnectListener(new DisconnectListener() {
			@Override
			public void onDisconnect(SocketIOClient arg0) {
				logger.debug("onDisconnect");
			}
		});

//		// Example of listener
//		server.addEventListener("test", String.class, new DataListener<String>() {
//			@Override
//			public void onData(SocketIOClient client, String message, AckRequest ack) {
//				logger.debug("test event");
//				logger.debug(message);
//			}
//		});
	}
	
	public void start() {
		logger.debug("start");
		server.start();
	}
	
	public void stop() {
		logger.debug("stop");
		server.stop();
	}
	
	public void broadcast(EVENT_NAME eventName, Object... data) {
		logger.debug("broadcast: " + eventName.toString());
		server.getBroadcastOperations().sendEvent(eventName.toString(), data);
	}
}
