(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('ReportController', [
        '$state', 'ReportService', 'flash', 'Socket', '$timeout',
        function ($state, ReportService, flash, Socket, $timeout) {
            var self = this;
            self.requests = [];
            
            self.$onInit = function(){
            	self.requests = ReportService.queryAll();
            };

            self.doRegisterReports = function () {
                ReportService.registerReports().$promise
                .then(
                    function (response) {
                        console.log(response);
                    },
                    function (response) {
                        console.log("error"+ response)
                    }
                );
            };
            
            Socket.on('report_request', function (data) {
                $timeout(function () {
                    self.requests.push(data);
                });
            });
        }
    ]);
}(this, angular));
