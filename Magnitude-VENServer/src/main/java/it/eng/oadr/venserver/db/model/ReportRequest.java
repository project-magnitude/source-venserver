package it.eng.oadr.venserver.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ReportRequest extends AbstractEntity implements Serializable{
	private static final long serialVersionUID = 3872692059161288297L;

	@Column(name="requestId")
	private String requestId;
	
	@Column(name="specifierId")
	private String specifierId;
	
	@Column(name="granularity")
	private String granularity;
	
	@Column(name="reportBackDuration")
	private String reportBackDuration;
	
	@Column(name="intervalStart")
	private Date intervalStart;
	
	@Column(name="intervalDuration")
	private String intervalDuration;
	
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getSpecifierId() {
		return specifierId;
	}
	public void setSpecifierId(String specifierId) {
		this.specifierId = specifierId;
	}
	public String getGranularity() {
		return granularity;
	}
	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}
	public String getReportBackDuration() {
		return reportBackDuration;
	}
	public void setReportBackDuration(String reportBackDuration) {
		this.reportBackDuration = reportBackDuration;
	}
	public Date getIntervalStart() {
		return intervalStart;
	}
	public void setIntervalStart(Date intervalStart) {
		this.intervalStart = intervalStart;
	}
	public String getIntervalDuration() {
		return intervalDuration;
	}
	public void setIntervalDuration(String intervalDuration) {
		this.intervalDuration = intervalDuration;
	}
}
