package it.eng.oadr.venserver.db.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Event extends AbstractEntity implements Serializable{
	private static final long serialVersionUID = -7831311727859303178L;

	@Column(name="eventId")
	private String eventId;
	
	@Column(name="modificationNumber")
	private long modificationNumber;
	
	@Column(name="priority")
	private long priority;
	
	@Column(name="marketContext")
	private String marketContext;

	@Column(name="createdDate")
	private Date createdDate;
	
	@Column(name="eventStatus")
	private String eventStatus;
	
	@Column(name="vtnComment")
	private String vtnComment;
	
	@Column(name="startTime")
	private Date startTime;
	
	@Column(name="duration")
	private String duration;
	
	@Column(name="startAfter")
	private String startAfter;
	
	@Column(name="eiNotification")
	private String eiNotification;
	
	@Column(name="eiRampUp")
	private String eiRampUp;
	
	@Column(name="eiRecovery")
	private String eiRecovery;
	
	@Column(name="responseRequired")
	private String responseRequired;
	
	@OneToMany(mappedBy = "event", orphanRemoval= true, cascade = CascadeType.ALL)
	private List<Signal> signals;
	
	public Event(){
		
	}
	
	public Event(long id, String eventId, Date startTime, Date createdDate, String duration, String eventStatus, String marketContext) {
		super();
		this.setId(id);
		this.eventId = eventId;
		this.createdDate = createdDate;
		this.marketContext = marketContext;
		this.eventStatus = eventStatus;
		this.startTime = startTime;
		this.duration = duration;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public long getModificationNumber() {
		return modificationNumber;
	}

	public void setModificationNumber(long modificationNumber) {
		this.modificationNumber = modificationNumber;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(long priority) {
		this.priority = priority;
	}

	public String getMarketContext() {
		return marketContext;
	}

	public void setMarketContext(String marketContext) {
		this.marketContext = marketContext;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}

	public String getVtnComment() {
		return vtnComment;
	}

	public void setVtnComment(String vtnComment) {
		this.vtnComment = vtnComment;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getStartAfter() {
		return startAfter;
	}

	public void setStartAfter(String startAfter) {
		this.startAfter = startAfter;
	}

	public String getEiNotification() {
		return eiNotification;
	}

	public void setEiNotification(String eiNotification) {
		this.eiNotification = eiNotification;
	}

	public String getEiRampUp() {
		return eiRampUp;
	}

	public void setEiRampUp(String eiRampUp) {
		this.eiRampUp = eiRampUp;
	}

	public String getEiRecovery() {
		return eiRecovery;
	}

	public void setEiRecovery(String eiRecovery) {
		this.eiRecovery = eiRecovery;
	}

	public String getResponseRequired() {
		return responseRequired;
	}

	public void setResponseRequired(String responseRequired) {
		this.responseRequired = responseRequired;
	}

	public List<Signal> getSignals() {
		return signals;
	}

	public void setSignals(List<Signal> signals) {
		this.signals = signals;
	}
}
