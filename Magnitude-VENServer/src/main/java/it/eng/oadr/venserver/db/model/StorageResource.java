package it.eng.oadr.venserver.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class StorageResource extends Resource{
	private static final long serialVersionUID = 1L;
	
	@Column(name="maxCapacity")
	private float maxCapacity;

	@Column(name="chargeRate")
	private float chargeRate;
	
	@Column(name="dischargeRate")
	private float dischargeRate;

	public float getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(float maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public float getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(float chargeRate) {
		this.chargeRate = chargeRate;
	}

	public float getDischargeRate() {
		return dischargeRate;
	}

	public void setDischargeRate(float dischargeRate) {
		this.dischargeRate = dischargeRate;
	}
}
