package it.eng.oadr.venserver.model;

import java.util.Date;

import org.oasis_open.docs.ns.energyinterop._201110.EiResponseType;
import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplyMessage {
	private static final Logger logger = LoggerFactory.getLogger(ReplyMessage.class);
	
	private long id;
	private Date date;
	private float responseTime;
	private String requestType;
	private String responseType;
	private String responseCode;
	private String responseMessage;
	
	private class OadrType {
		private String type;
		private EiResponseType eiResponseType;
		public OadrType(String type, EiResponseType eiResponseType) {
			this.type = type;
			this.eiResponseType = eiResponseType;
		}
		public String getType() {
			return this.type;
		}
		public EiResponseType getEiResponse() {
			return this.eiResponseType;
		}
	}

	public ReplyMessage() {
	}

	public ReplyMessage(OadrPayload reqPayload, OadrPayload resPayload) {
		this.requestType = deduceOadrType(reqPayload).getType();
		
		OadrType oadrType = deduceOadrType(resPayload);
		this.responseType = oadrType.getType();
		if (oadrType.getEiResponse() != null) {
			this.responseCode = oadrType.getEiResponse().getResponseCode();
			this.responseMessage = oadrType.getEiResponse().getResponseDescription();
		}
	}

	private OadrType deduceOadrType(OadrPayload payload) {
		logger.debug("deduceOadrType");

		if (payload == null || payload.getOadrSignedObject() == null) {
			return null;
		}

		OadrSignedObject signed = payload.getOadrSignedObject();
		if (signed.getOadrResponse() != null) {
			return new OadrType("oadrResponse", signed.getOadrResponse().getEiResponse());
		}

		if (signed.getOadrQueryRegistration() != null) {
			return new OadrType("oadrQueryRegistration", null);
		}

		if (signed.getOadrCreatePartyRegistration() != null) {
			return new OadrType("oadrCreatePartyRegistration", null);
		}
		if (signed.getOadrCreatedPartyRegistration() != null) {
			return new OadrType("oadrCreatedPartyRegistration", signed.getOadrCreatedPartyRegistration().getEiResponse());
		}

		if (signed.getOadrCancelPartyRegistration() != null) {
			return new OadrType("oadrCancelPartyRegistration", null);
		}
		if (signed.getOadrCanceledPartyRegistration() != null) {
			return new OadrType("oadrCanceledPartyRegistration", signed.getOadrCanceledPartyRegistration().getEiResponse());
		}

		if (signed.getOadrRequestEvent() != null) {
			return new OadrType("oadrRequestEvent", null);
		}
		if (signed.getOadrDistributeEvent() != null) {
			return new OadrType("oadrDistributeEvent", signed.getOadrDistributeEvent().getEiResponse());
		}
		if (signed.getOadrCreatedEvent() != null) {
			return new OadrType("oadrCreatedEvent", signed.getOadrCreatedEvent().getEiCreatedEvent().getEiResponse());
		}

		if (signed.getOadrRequestReregistration() != null) {
			return new OadrType("oadrRequestReregistration", null);
		}

		if (signed.getOadrRegisterReport() != null) {
			return new OadrType("oadrRegisterReport", null);
		}
		if (signed.getOadrRegisteredReport() != null) {
			return new OadrType("oadrRegisteredReport", signed.getOadrRegisteredReport().getEiResponse());
		}

		if (signed.getOadrCreateReport() != null) {
			return new OadrType("oadrCreateReport", null);
		}
		if (signed.getOadrCreatedReport() != null) {
			return new OadrType("oadrCreatedReport", signed.getOadrCreatedReport().getEiResponse());
		}

		if (signed.getOadrUpdateReport() != null) {
			return new OadrType("oadrUpdateReport", null);
		}
		if (signed.getOadrUpdatedReport() != null) {
			return new OadrType("oadrUpdatedReport", signed.getOadrUpdatedReport().getEiResponse());
		}
		
		if (signed.getOadrCancelReport() != null) {
			return new OadrType("oadrCancelReport", null);
		}
		if (signed.getOadrCanceledReport() != null) {
			return new OadrType("oadrCanceledReport", signed.getOadrCanceledReport().getEiResponse());
		}
		
		if (signed.getOadrCreateOpt() != null) {
			return new OadrType("oadrCreateOpt", null);
		}
		if (signed.getOadrCreatedOpt() != null) {
			return new OadrType("oadrCreatedOpt", signed.getOadrCreatedOpt().getEiResponse());
		}
		
		if (signed.getOadrCancelOpt() != null) {
			return new OadrType("oadrCancelOpt", null);
		}
		if (signed.getOadrCanceledOpt() != null) {
			return new OadrType("oadrCanceledOpt", signed.getOadrCanceledOpt().getEiResponse());
		}

		if (signed.getOadrPoll() != null) {
			return new OadrType("oadrPoll", null);
		}
		
		logger.error("Unknown type name");
		return new OadrType("", null);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(float responseTime) {
		this.responseTime = responseTime;
	}
}
