package it.eng.oadr.venserver.services;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.openadrmodel.Parser;
import it.eng.oadr.venserver.db.Dao;
import it.eng.oadr.venserver.db.model.Log;
import it.eng.oadr.venserver.impl.Oadr20bImpl;
import it.eng.oadr.venserver.model.ReplyMessage;
import it.eng.oadr.venserver.socket.SocketServer;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
@Path("Simple/2.0b")
public class Simple20b {

	private static final Logger log = LoggerFactory.getLogger(Simple20b.class);

	@POST
	@Path("EiEvent")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiEvent(String oadrPayload) {
		log.debug("post EiEvent");
		log.debug("input: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			OadrSignedObject so = in.getOadrSignedObject();
			if (so.getOadrDistributeEvent() != null) {
				// Oadr20bImpl.getInstance().onOadrDistributeEvent(so.getOadrDistributeEvent());
				out.getOadrSignedObject()
						.setOadrResponse(Oadr20bImpl.getInstance().onOadrDistributeEvent(so.getOadrDistributeEvent()));
			}
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		try {
			saveLog(in, out);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		return ret;
	}

	@POST
	@Path("EiRegisterParty")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiRegisterParty(String oadrPayload) {
		log.debug("post EiRegisterParty");
		log.debug("input: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			OadrSignedObject so = in.getOadrSignedObject();
			if (so.getOadrCancelPartyRegistration() != null) {
				out.getOadrSignedObject().setOadrCanceledPartyRegistration(
						Oadr20bImpl.getInstance().onOadrCancelPartyRegistration(so.getOadrCancelPartyRegistration()));
			} else if (so.getOadrRequestReregistration() != null) {
				out.getOadrSignedObject().setOadrResponse(
						Oadr20bImpl.getInstance().onOadrRequestReregistration(so.getOadrRequestReregistration()));
			}
			if (so.getOadrCreatedPartyRegistration() != null) {
				out.getOadrSignedObject().setOadrResponse(
						Oadr20bImpl.getInstance().onOadrCreatedPartyRegistration(so.getOadrCreatedPartyRegistration()));
			}
			if (so.getOadrCanceledPartyRegistration() != null) {
				out.getOadrSignedObject().setOadrResponse(Oadr20bImpl.getInstance()
						.onOadrCanceledPartyRegistration(so.getOadrCanceledPartyRegistration()));
			}
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		try {
			saveLog(in, out);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		return ret;
	}

	@POST
	@Path("EiReport")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiReport(String oadrPayload) {
		log.debug("post EiReport");
		log.debug("input: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			OadrSignedObject so = in.getOadrSignedObject();
			if (so.getOadrRegisterReport() != null) {
				out.getOadrSignedObject().setOadrRegisteredReport(
						Oadr20bImpl.getInstance().onOadrRegisterReport(so.getOadrRegisterReport()));
			} else if (so.getOadrCreateReport() != null) {
				out.getOadrSignedObject()
						.setOadrCreatedReport(Oadr20bImpl.getInstance().onOadrCreateReport(so.getOadrCreateReport()));
			} else if (so.getOadrUpdateReport() != null) {
				out.getOadrSignedObject()
						.setOadrUpdatedReport(Oadr20bImpl.getInstance().onOadrUpdateReport(so.getOadrUpdateReport()));
			} else if (so.getOadrCancelReport() != null) {
				out.getOadrSignedObject()
						.setOadrCanceledReport(Oadr20bImpl.getInstance().onOadrCancelReport(so.getOadrCancelReport()));
			} else if (so.getOadrRegisteredReport() != null) {
				out.getOadrSignedObject().setOadrResponse(
						Oadr20bImpl.getInstance().onOadrRegisteredReport(so.getOadrRegisteredReport()));
			} else if (so.getOadrUpdatedReport() != null) {
				out.getOadrSignedObject()
						.setOadrResponse(Oadr20bImpl.getInstance().onOadrUpdatedReport(so.getOadrUpdatedReport()));
			}
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		try {
			saveLog(in, out);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		return ret;
	}

	@POST
	@Path("EiOpt")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postEiOpt(String oadrPayload) {
		log.debug("post EiOpt");
		log.debug("input: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			OadrSignedObject so = in.getOadrSignedObject();
			if (so.getOadrCreatedOpt() != null) {
				out.getOadrSignedObject()
						.setOadrResponse(Oadr20bImpl.getInstance().onOadrCreatedOpt(so.getOadrCreatedOpt()));
			} else if (so.getOadrCanceledOpt() != null) {
				out.getOadrSignedObject()
						.setOadrResponse(Oadr20bImpl.getInstance().onOadrCanceledOpt(so.getOadrCanceledOpt()));
			}

		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		try {
			saveLog(in, out);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}

		return ret;
	}

	@POST
	@Path("OadrPoll")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public synchronized String postOadrPoll(String oadrPayload) {
		log.debug("post OadrPoll");
		log.debug("input: " + oadrPayload);
		OadrPayload in = null;
		try {
			in = (OadrPayload) Parser.unmarshal(oadrPayload);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		OadrPayload out = new OadrPayload();
		out.setOadrSignedObject(new OadrSignedObject());
		if (in != null && in.getOadrSignedObject() != null) {
			OadrSignedObject so = in.getOadrSignedObject();
			if (so.getOadrPoll() != null) {
				out.getOadrSignedObject().setOadrResponse(Oadr20bImpl.getInstance().onOadrPoll(so.getOadrPoll()));
			}
		}

		String ret = null;
		try {
			ret = Parser.marshal(out);
			log.debug("output: " + ret);
		} catch (JAXBException ex) {
			log.error(ex.getMessage(), ex);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		return ret;
	}

	private void saveLog(OadrPayload reqPayload, OadrPayload resPayload) throws JAXBException, Exception {
		Log entity = new Log();
		Date now = new Date();
		entity.setDate(now);

		String reqString = Parser.marshal(reqPayload);
		entity.setRequestPayload(reqString);

		Date end = new Date();

		ReplyMessage message = new ReplyMessage(reqPayload, resPayload);

		entity.setRequestType(message.getRequestType());
		if (message.getResponseCode() != null) {
			entity.setResponseCode(Long.parseLong(message.getResponseCode()));
		}
		entity.setResponseMessage(message.getResponseMessage());
		String resString = Parser.marshal(resPayload);
		entity.setResponsePayload(resString);
		float seconds = (float) (end.getTime() - now.getTime()) / (float) 1000;

		entity.setResponseTime(seconds);
		entity.setResponseType(message.getResponseType());

		Dao<Log> dao = new Dao<>(Log.class);
		// scartiamo i messaggi di Polling
		if (!entity.getRequestType().equals("oadrPoll")) {
			dao.persist(entity);

			message.setId(entity.getId());
			message.setDate(entity.getDate());
			message.setResponseTime(entity.getResponseTime());

			SocketServer.INSTANCE.broadcast(SocketServer.EVENT_NAME.POLLING, message);
		}
		return;
	}

}
