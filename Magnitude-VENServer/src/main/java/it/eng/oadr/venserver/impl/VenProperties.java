package it.eng.oadr.venserver.impl;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.openadrclient.Oadr20bClient;

public class VenProperties {

	private static final Logger logger = LoggerFactory.getLogger(VenProperties.class);
	private static Properties props = null;

	public static synchronized String getProperty(String key) throws Exception {
		String ret = null;
		if (props == null) {
			props = new Properties();
			final String ELSA_ENV = System.getenv("ELSA_ENV");
			String propertyName = "/ven";
			if (ELSA_ENV != null && !ELSA_ENV.equals("")) {
				propertyName += "." + ELSA_ENV;
			}
			propertyName += ".properties";
			logger.debug("propertyName: " + propertyName);
			props.load(Oadr20bClient.class.getResourceAsStream(propertyName));
		}
		ret = props.getProperty(key);
		if (ret == null) {
			throw new Exception("Property " + key + " not found");
		}
		return ret;
	}

}
