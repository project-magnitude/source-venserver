=====================================================
Source Code of VEN Server implementation - v1.0
=====================================================


.:: CONTENTS ::.

This project contains source code for the VEN Server


.:: SETUP ::.

To generate the corresponding war file use maven with the pom.xml


.:: CHANGELOG ::.

v1.0 Tested on the development machine


.:: CONTACTS ::.

alessio.roppolo@eng.it
