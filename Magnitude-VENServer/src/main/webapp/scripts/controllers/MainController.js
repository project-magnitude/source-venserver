(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('MainController', [
        '$state',
        function ($state) {
            var self = this;

            self.state = $state;
        }
    ]);
}(this, angular));
