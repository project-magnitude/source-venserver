package it.eng.oadr.venserver.services;

import java.util.concurrent.ExecutionException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import it.eng.oadr.venserver.impl.Oadr20bImpl;
import it.eng.oadr.venserver.model.RegistrationInfo;
import it.eng.oadr.venserver.model.ReplyMessage;

@Path("ven")
public class GenericVen {
	
	private static final Logger logger = LoggerFactory.getLogger(GenericVen.class);
	
	@GET
	@Path("oadrQueryRegistration")
	@Produces(MediaType.APPLICATION_JSON)
	public String oadrQueryRegistration(@QueryParam("url") String url){
		logger.debug("server url"+url);
		long frequency = Oadr20bImpl.getInstance().doQueryRegistration(url);
		String freq = Long.toString(frequency);
		Gson gson = new Gson();
		return gson.toJson(freq);
	}
	
	@POST
	@Path("oadrCreatePartyRegistration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String oadrCreatePartyRegistration(String registrationInfo) {
		logger.debug("registrationInfo: " + registrationInfo);
		Gson gson = new Gson();
		RegistrationInfo info = gson.fromJson(registrationInfo, RegistrationInfo.class);
		String registrationId = Oadr20bImpl.getInstance().doCreatePartyRegistration(info);
		return gson.toJson(registrationId);
	}

	@POST
	@Path("oadrCancelPartyRegistration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String oadrCancelPartyRegistration() {
		Gson gson = new Gson();
		Oadr20bImpl.getInstance().doCancelPartyRegistration();
		return gson.toJson(null);
	}

	@POST
	@Path("oadrStartPolling")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String oadrStartPolling(String registrationInfo) throws InterruptedException, ExecutionException {
		logger.debug("registrationInfo: " + registrationInfo);
		Gson gson = new Gson();
		RegistrationInfo info = gson.fromJson(registrationInfo, RegistrationInfo.class);
		ReplyMessage message = null;
		try {
			message = Oadr20bImpl.getInstance().startPolling(info);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return gson.toJson(message, ReplyMessage.class);
	}

	@POST
	@Path("oadrStopPolling")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String oadrStopPolling() {
		Gson gson = new Gson();
		Oadr20bImpl.getInstance().stopPolling();
		ReplyMessage message = new ReplyMessage();
		message.setResponseCode("200");
		message.setResponseMessage("OK");
		return gson.toJson(message, ReplyMessage.class);
	}

	@POST
	@Path("oadrTogglePolling")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response oadrTogglePolling(String registrationInfo) throws InterruptedException, ExecutionException {
		logger.debug("registrationInfo: " + registrationInfo);
		Gson gson = new Gson();
		RegistrationInfo info = gson.fromJson(registrationInfo, RegistrationInfo.class);
		boolean isPollRunning = false;
		try {
			isPollRunning = Oadr20bImpl.getInstance().togglePolling(info);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("isPollRunning: " + isPollRunning);
		Boolean entity = Boolean.valueOf(isPollRunning);
		logger.debug("entity: " + entity);
		return Response.ok(entity).build();
	}

	@GET
	@Path("properties")
	@Produces(MediaType.APPLICATION_JSON)
	public String getProperties(){
		Gson gson = new Gson();
		RegistrationInfo info = Oadr20bImpl.getInstance().getRegistrationInfo();
		return gson.toJson(info);
	}
	
	
}
