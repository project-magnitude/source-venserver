package it.eng.oadr.venserver.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class OadrRequestReregistrationRunnable implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(OadrRequestReregistrationRunnable.class);

    public OadrRequestReregistrationRunnable() {
    }

    @Override
    public void run() {
        try {
            //Oadr20bImpl.getInstance().registerVEN();
        	Oadr20bImpl.getInstance().doCreatePartyRegistration();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
}
