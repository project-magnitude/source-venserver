(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('ReportService', [
        '$resource', 'BASE_PATH',
        function ($resource, basePath) {
        	var report = $resource(basePath + '/OpenADR2/reports/:id',
                    {
                        id: '@id'
                    },
                    {
                        queryAll: {
                            method: 'GET',
                            isArray: true
                        },
                        query: {
                            method: 'GET',
                            isArray: false
                        },
                        update: {
                            method: 'PUT'
                        },
                     	registerReports: {
                            method: 'POST',
                            url: basePath + '/OpenADR2/reports/registerReport'
                     	}
                    });
            return report;
        }
    ]);
}(this, angular));