package it.eng.oadr.venserver.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.venserver.db.model.ReportRequest;
import it.eng.oadr.venserver.impl.Oadr20bImpl;

@Path("reports")
public class ReportEndpoint extends AbstractEndpoint<ReportRequest>{
	
	public ReportEndpoint(){
		super(ReportRequest.class);
	}
	
	private static final Logger log = LoggerFactory.getLogger(ReportEndpoint.class);
	
	@POST
	@Path("registerReport")
	@Consumes("application/json")
	@Produces("application/json")
	public Response oadrRegisterReport(){
		//OadrRegisteredReportType response;
        OadrPayload response;
		try {
			response = Oadr20bImpl.getInstance().registerReportCustomResources();
			if(response.getOadrSignedObject().getOadrResponse().equals("200")){
				return Response.status(200).build();
			}
			
		} catch (Exception e) {
			return Response.status(Status.EXPECTATION_FAILED).build();
		}
		return Response.noContent().build();
	}

}
