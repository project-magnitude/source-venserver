(function (window, angular) {
    'use strict';

    // https://github.com/PrismJS/prism/issues/612
    angular.module('Directives')
    .directive('codeHighlight', [
        '$compile', '$timeout',
        function ($compile, $timeout) {
            return {
                restrict: 'E',
                scope: {
                    preClass: '@',
                    codeClass: '@',
                    source: '@'
                },
                link: function (scope, element) {
                    var timeout;
                    scope.$watch('source', function (value) {
                        if (!value) {
                            return;
                        }

                        element.html('<pre class="{{preClass}}"><code class="{{codeClass}}">{{ source }}</code>');
                        $compile(element.contents())(scope);

                        var code = element.find('code')[0];
                        timeout = $timeout(function () {
                            Prism.highlightElement(code);
                        }, 0, false);
                    });

                    scope.$on('$destroy', function () {
                        $timeout.cancel(timeout);
                    });
                }
            };
        }
    ]);
}(this, angular));
