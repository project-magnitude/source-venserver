package it.eng.oadr.venserver.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Log extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 7603948116726933820L;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="responseTime")
	private float responseTime;
	
	@Column(name="requestType")
	private String requestType;
	
	@Column(name="requestPayload", columnDefinition="CLOB NOT NULL")
	private String requestPayload;
	
	@Column(name="responseType")
	private String responseType;
	
	@Column(name="responsePayload", columnDefinition="CLOB NOT NULL")
	private String responsePayload;
	
	@Column(name="responseCode")
	private long responseCode;
	
	@Column(name="responseMessage")
	private String responseMessage;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(float responseTime) {
		this.responseTime = responseTime;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getRequestPayload() {
		return requestPayload;
	}

	public void setRequestPayload(String requestPayload) {
		this.requestPayload = requestPayload;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getResponsePayload() {
		return responsePayload;
	}

	public void setResponsePayload(String responsePayload) {
		this.responsePayload = responsePayload;
	}

	public long getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(long responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
