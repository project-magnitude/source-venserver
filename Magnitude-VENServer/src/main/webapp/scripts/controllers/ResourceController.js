(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('ResourceController', [
        '$state', '$stateParams', 'flash', 'ResourceService',
        function ($state, $stateParams, flash, ResourceService) {
            var self = this;

            self.resourceTypes = [
                {
                    group: 'Available types',
                    label: 'Load',
                    value: 'load'
                },
                {
                    group: 'Available types',
                    label: 'Generator',
                    value: 'generator'
                },
                {
                    group: 'Available types',
                    label: 'Storage',
                    value: 'storage'
                }
            ];
            self.formData = {};
            self.isCreating = false;

            self.get = function (id) {
                var successCallback = function (data) {
                    self.original = data;
                    self.formData = new ResourceService(self.original);
                };
                var errorCallback = function () {
                    flash.setError('The resource could not be found.');
                    $state.go('resources.search');
                };
                ResourceService.get({ id: id }, successCallback, errorCallback);
            };

            self.create = function () {
                var successCallback = function (data, responseHeaders) {
                    flash.setSuccess('The resource was created successfully.');
                    $state.go('resources.search', null, { reload: true });
                };
                var errorCallback = function (response) {
                    if (response && response.data && response.data.message) {
                        flash.setError(response.data.message, true);
                    } else {
                        flash.setError('Something broke. Retry, or cancel and start afresh.', true);
                    }
                };
                ResourceService.save(self.formData, successCallback, errorCallback);
            };

            self.update = function () {
                var successCallback = function () {
                    flash.setSuccess('The resource was updated successfully.', true);
                    self.get(self.formData.id);
                };
                var errorCallback = function (response) {
                    if (response && response.data && response.data.message) {
                        flash.setError(response.data.message, true);
                    } else {
                        flash.setError('Something broke. Retry, or cancel and start afresh.', true);
                    }
                };
                self.formData.$update(successCallback, errorCallback);
            };

            self.remove = function () {
                var successCallback = function () {
                    flash.setError('The resource was deleted.');
                    $state.go("resources.search");
                };
                var errorCallback = function (response) {
                    if (response && response.data && response.data.message) {
                        flash.setError(response.data.message, true);
                    } else {
                        flash.setError('Something broke. Retry, or cancel and start afresh.', true);
                    }
                };
                self.formData.$remove(successCallback, errorCallback);
            };

            self.cancel = function () {
                $state.go('resources.search');
            };

            self.isClean = function() {
                return angular.equals(self.original, self.formData);
            };

            self.$onInit = function () {
                var id = $stateParams.id;
                if (id !== '') {
                    self.get(id);
                    self.save = self.update;
                    self.isCreating = false;
                } else {
                    self.save = self.create;
                    self.isCreating = true;
                }
            };
        }
    ]);
}(this, angular));
