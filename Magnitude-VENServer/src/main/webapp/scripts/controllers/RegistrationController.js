(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('RegistrationController', [
        '$timeout', '$q', '$log', 'flash', 'RegistrationService', 'LogService', 'Socket',
        function ($timeout, $q, $log, flash, RegistrationService, LogService, Socket) {
            var self = this;

            self.registerParams = {};
            self.messages = [];

            self.$onInit = function () {
                self.isPolling = false;
                var promises = {
                    props: RegistrationService.getInitialProperties(),
                    isPolling: LogService.isPollingRunning()
                };
                $q.all(promises)
                .then(function (response) {
                    $timeout(function () {
                        self.registerParams = response.props.data;
                        self.isPolling = response.isPolling.data;
                    });
                    // This query can be expensive: do it in a separate call.
                    return LogService.queryAll();
                })
                .then(function (logs) {
                    $timeout(function () {
                        self.messages = logs;
                    });
                })
                .catch(function (response) {
                    $log.error(response.data);
                });
            };

            self.doQueryRegistration = function () {
                var url = self.registerParams.target;
                RegistrationService.queryRegistration(url)
                .then(
                    function (response) {
                        self.pollFreq = response.data;
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            self.doCreatePartyRegistration = function () {
                RegistrationService.createPartyRegistration(self.registerParams)
                .then(
                    function (response) {
                        self.registrationId = response.data;
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            self.doCancelPartyRegistration = function () {
                RegistrationService.cancelPartyRegistration()
                .then(
                    function (response) {
                        flash.setSuccess('CancelPartyRegistration called', true);
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            self.doStartPolling = function () {
                RegistrationService.startPolling(self.registerParams)
                .then(
                    function (response) {
                        flash.setSuccess('StartPolling called', true);
                        self.isPolling = true;
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            self.doStopPolling = function () {
                RegistrationService.stopPolling()
                .then(
                    function (response) {
                        flash.setSuccess('StopPolling called', true);
                        self.isPolling = false;
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            self.togglePolling = function () {
                RegistrationService.togglePolling(self.registerParams)
                .then(
                    function (response) {
                        flash.setSuccess('Polling ' + (response.data ? 'started' : 'stopped'), true);
                        self.isPolling = response.data;
                    },
                    function (response) {
                        $log.error(response.data);
                    }
                );
            };

            Socket.on('polling', function (data) {
                $timeout(function () {
                    self.messages.push(data);
                });
            });

            self.showDetails = function (item) {
                function successCallback(data) {
                    self.logItem = data;
                }
                function errorCallback(error) {
                    flash.setError('The log could not be found.');
                }
                LogService.get({ id: item.id }, successCallback, errorCallback);
            };
        }
    ]);
}(this, angular));
