(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('ResourceService', [
        '$resource', 'BASE_PATH',
        function ($resource, basePath) {
            var resource = $resource(basePath + '/OpenADR2/resources/:id',
                {
                    id: '@id'
                },
                {
                    queryAll: {
                        method: 'GET',
                        isArray: true
                    },
                    query: {
                        method: 'GET',
                        isArray: false
                    },
                    update: {
                        method: 'PUT'
                    }
                });
            return resource;
        }
    ]);
}(this, angular));
