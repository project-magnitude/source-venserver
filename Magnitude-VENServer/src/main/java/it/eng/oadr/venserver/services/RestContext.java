package it.eng.oadr.venserver.services;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.venserver.db.EntityManagerHelper;
import it.eng.oadr.venserver.impl.Oadr20bImpl;
import it.eng.oadr.venserver.socket.SocketServer;

@WebListener
public class RestContext implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(RestContext.class);

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
    	logger.debug("-->contextDestroyed");

    	logger.debug("Close OpenADR");
        Oadr20bImpl.getInstance().doCancelReport();
        Oadr20bImpl.getInstance().doCancelPartyRegistration();
        Oadr20bImpl.getInstance().doResetPoll();
        
        // disattiva i task attivati
        Oadr20bImpl.getInstance().shutdown();
        
        logger.debug("Close EntityManager");
        EntityManagerHelper.closeEntityManager();
        EntityManagerHelper.closeEntityManagerFactory();
        
        logger.debug("Close Socket");
        SocketServer.INSTANCE.stop();
	}

}
