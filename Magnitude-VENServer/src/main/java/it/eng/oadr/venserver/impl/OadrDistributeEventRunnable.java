package it.eng.oadr.venserver.impl;

import org.oasis_open.docs.ns.energyinterop._201110.EiEventSignalType;
import org.oasis_open.docs.ns.energyinterop._201110.EiResponseType;
import org.oasis_open.docs.ns.energyinterop._201110.EventResponses;
import org.oasis_open.docs.ns.energyinterop._201110.OptTypeType;
import org.oasis_open.docs.ns.energyinterop._201110.QualifiedEventIDType;
import org.oasis_open.docs.ns.energyinterop._201110.payloads.EiCreatedEvent;
import org.openadr.oadr_2_0b._2012._07.OadrCreatedEventType;
import org.openadr.oadr_2_0b._2012._07.OadrDistributeEventType;
import org.openadr.oadr_2_0b._2012._07.OadrResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class OadrDistributeEventRunnable implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(OadrDistributeEventRunnable.class);
    private OadrDistributeEventType oDistributeEvent = null;

    public OadrDistributeEventRunnable(OadrDistributeEventType oDistributeEvent) {
        this.oDistributeEvent = oDistributeEvent;
    }

    @Override
    public void run() {
        try {
            OadrCreatedEventType oCreatedEvent = new OadrCreatedEventType();
            oCreatedEvent.setEiCreatedEvent(new EiCreatedEvent());
            EventResponses eventResponses = new EventResponses();
            
            for (OadrDistributeEventType.OadrEvent oEvent : oDistributeEvent.getOadrEvent()) {
                EventResponses.EventResponse eventResponse = new EventResponses.EventResponse();
                eventResponse.setRequestID(oDistributeEvent.getRequestID());
                eventResponse.setQualifiedEventID(new QualifiedEventIDType());
                eventResponse.getQualifiedEventID().setEventID(oEvent.getEiEvent().getEventDescriptor().getEventID());
                eventResponse.getQualifiedEventID().setModificationNumber(oEvent.getEiEvent().getEventDescriptor().getModificationNumber());
                
                //TODO: aggiungere interazione con "Intra-DC Real-Time Multi-Criteria Energy Optimiser"
                log.debug("processing eventID=" + oEvent.getEiEvent().getEventDescriptor().getEventID());
                for (EiEventSignalType signal : oEvent.getEiEvent().getEiEventSignals().getEiEventSignal()) {
                    log.debug("processing signalID=" + signal.getSignalID() + "; signalType=" + signal.getSignalType());
                }
                eventResponse.setResponseCode("200");
                eventResponse.setResponseDescription("OK");
                eventResponse.setOptType(OptTypeType.OPT_IN);
                
                eventResponses.getEventResponse().add(eventResponse);
            }

            oCreatedEvent.getEiCreatedEvent().setEventResponses(eventResponses);
            EiResponseType eiResponse = new EiResponseType();
            eiResponse.setResponseCode("200");
            eiResponse.setResponseDescription("OK");
            eiResponse.setRequestID("");
            oCreatedEvent.getEiCreatedEvent().setEiResponse(eiResponse);
            OadrResponseType oResponse = Oadr20bImpl.getInstance().sendOadrCreatedEvent(oCreatedEvent);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

}
