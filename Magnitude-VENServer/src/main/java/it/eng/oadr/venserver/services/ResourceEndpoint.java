package it.eng.oadr.venserver.services;

import javax.ws.rs.Path;

import it.eng.oadr.venserver.db.model.Resource;

@Path("resources")
public class ResourceEndpoint extends AbstractEndpoint<Resource> {
	
	public ResourceEndpoint() {
		super(Resource.class);
	}
	
	@Override
	protected Resource parseEntity(String json) {
		Resource r = Resource.resourceFactory(json);
		return r;
	};

}
