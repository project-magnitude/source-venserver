
package it.eng.oadr.openadrmodel;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class Parser.
 */
public class Parser {

    /** The Constant log. */
    private static final Logger log = LoggerFactory.getLogger(Parser.class);
    
    /** The marshaller. */
    private static Marshaller marshaller = null;
    
    /** The unmarshaller. */
    private static Unmarshaller unmarshaller = null;

    static {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(
                    "ietf.params.xml.ns.icalendar_2:"
                            + "ietf.params.xml.ns.icalendar_2_0.stream:"
                            + "net.opengis.gml._3:"
                            + "org.naesb.espi:"
                            + "org.oasis_open.docs.ns.emix._2011._06:"
                            + "org.oasis_open.docs.ns.emix._2011._06.power:"
                            + "org.oasis_open.docs.ns.emix._2011._06.siscale:"
                            + "org.oasis_open.docs.ns.energyinterop._201110:"
                            + "org.oasis_open.docs.ns.energyinterop._201110.payloads:"
                            + "org.openadr.oadr_2_0b._2012._07:"
                            + "org.w3._2000._09.xmldsig_:"
                            + "org.w3._2005.atom:"
                            + "org.w3._2009.xmldsig11_:"
                            + "un.unece.uncefact.codelist.standard._5.iso42173a._2010_04_07");
            log.debug("jaxbContext: " + jaxbContext);
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            unmarshaller = jaxbContext.createUnmarshaller();
            log.debug("marshaller: " + marshaller);
            log.debug("unmarshaller: " + unmarshaller);
        } catch (JAXBException ex) {
            log.error(ex.getMessage(), ex);
        }
    }
    
    /**
	 * Marshal.
	 *
	 * @param obj
	 *            the obj
	 * @return the string
	 * @throws JAXBException
	 *             the JAXB exception
	 */
    public static String marshal(Object obj) throws JAXBException {
    	synchronized(marshaller){
    		log.debug("-> synchronized marshal");
    		java.io.StringWriter writer = new java.io.StringWriter();
    		marshaller.marshal(obj, writer);
    		String ret = writer.toString();
    		log.debug("ret: " + ret);
    		return ret;
    	}
    }

	/**
	 * Unmarshal.
	 *
	 * @param xml
	 *            the xml
	 * @return the object
	 * @throws JAXBException
	 *             the JAXB exception
	 */
	public static Object unmarshal(String xml) throws JAXBException {
		synchronized (unmarshaller) {
			log.debug("-> synchronized unmarshal");
			Object ret = null;
			java.io.StringReader reader = new java.io.StringReader(xml);
			ret = unmarshaller.unmarshal(reader);
			log.debug("ret: " + ret);
			return ret;
		}
	}
}
