package it.eng.oadr.venserver.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.eng.oadr.venserver.db.EventDao;
import it.eng.oadr.venserver.db.model.Event;

@Path("events")
public class EventEndpoint extends AbstractEndpoint<Event>{
	
	protected EventDao daoEvent;
	
	public EventEndpoint(){
		super(Event.class);
		super.dao = new EventDao();
	}
}
