package it.eng.oadr.venserver.services;

import javax.ws.rs.Consumes;

import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.openadr.oadr_2_0b._2012._07.*;

import org.oasis_open.docs.ns.energyinterop._201110.*;
import it.eng.oadr.venserver.impl.Oadr20bImpl;
import it.eng.oadr.venserver.impl.RunOadrPollTask;
import it.eng.oadr.openadrmodel.Parser;
import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import ietf.params.xml.ns.icalendar_2_0.stream.*;
import ietf.params.xml.ns.icalendar_2_0.*;
import java.io.StringReader;
import java.util.*;
import javax.xml.datatype.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openadr.oadr_2_0b._2012._07.OadrPayload;
import org.openadr.oadr_2_0b._2012._07.OadrSignedObject;

/**
 *
 * @author Marco Guarini
 */
@Path("openAdrVen")
public class OpenAdrVen {

    private static final Logger log = LoggerFactory.getLogger(OpenAdrVen.class);

    @GET
    @Path("oadrDistributeEvent/{marketContext}/next")
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String getNextOadrEvent(
    		 @PathParam("marketContext") String marketContext) {
        log.debug("get getNextOadrEvent");
        log.debug("input: " + marketContext);
        String myMarketContext = marketContext.replaceAll("!", "/");
        String ret = null;
        // estrae il parametro <marketContext> dal URL 
        
        // cerca la coda FIFO associata alla key
        //      (<marketContext>,"OadrDistributeEvent", "QUEUE")
        // se non trova la coda FIFO or la coda FIFO è vuota
        
        log.debug("key: " + myMarketContext);
        log.debug("LIST_MARKET_CONTEXT: " + Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.toString());
        log.debug("test: LIST_MARKET_CONTEXT.contains(myMarketContext): " + Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext));
        
        
        if (!Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext) ||
        	Oadr20bImpl.getInstance().mapQueueOadrDistributeEvent == null || 
        	!Oadr20bImpl.getInstance().mapQueueOadrDistributeEvent.containsKey(myMarketContext) ||
        	Oadr20bImpl.getInstance().mapQueueOadrDistributeEvent.get(myMarketContext).isEmpty()){
        	///	  ritorna una EiResponseType con HTTP code 200 OK
        		EiResponseType eiResponse = new EiResponseType();
        		eiResponse.setResponseCode("200");
        		eiResponse.setResponseDescription("OK");
        		OadrResponseType oadrResponse = new OadrResponseType();
        		oadrResponse.setEiResponse(eiResponse);
        		oadrResponse.setSchemaVersion(Oadr20bImpl.SCHEMA_VERSION);
        		oadrResponse.setVenID(Oadr20bImpl.VEN_ID);
        		
        		try {
					//ret = Parser.marshal(out);
        			//effettua il marshal dell'oggetto out di tipo OadrResponseType 
//        			QName qName = new QName("oadrResponse");
//        	        JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
        			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrResponse(oadrResponse);
        			ret = Parser.marshal(root);
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        } else {
        	log.debug("get mapQueueOadrDistributeEvent: " + Oadr20bImpl.getInstance().mapQueueOadrDistributeEvent.get(myMarketContext));
        // altrimenti (coda trovata)
        //    estrae dalla coda FIFO  un item OadrDistributeEventType
        	  OadrDistributeEventType myOadrDistributeEvent = Oadr20bImpl.getInstance().mapQueueOadrDistributeEvent.get(myMarketContext).remove();   
        //
        //    recupera il oadrPayload:oadrSignedObject:oadrDistributeEvent:requestID
        //
        	  String myRequestID = myOadrDistributeEvent.getRequestID();
        	  
        //    se non esiste la coda con key (<marketContext>,"OadrDistributeEvent", "MAP") 
        //    (private static Map<String, Map<String, OadrDistributeEventType>> mapMapOadrDistributeEvent = null;)
        // 		 crea una hashmap
        //    altrimenti (hashmap trovata)
        //      inserisce l'item OadrDistributeEventType corrente nell'hashmap
        //      utilizzando come key(requestID)
        //    
        	  if (Oadr20bImpl.getInstance().mapMapOadrDistributeEvent == null) {
        		  Oadr20bImpl.getInstance().mapMapOadrDistributeEvent = new HashMap<String, Map<String, OadrDistributeEventType>>();
        		                                                       
        	  }		
              if (!Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.containsKey(myMarketContext)) { 	
            	 HashMap<String, OadrDistributeEventType> mapOadrDistributeEvent = new HashMap<String, OadrDistributeEventType>();
            	  Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.put(myMarketContext,mapOadrDistributeEvent);
              }
              
              Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.get(myMarketContext).put(myRequestID, myOadrDistributeEvent);   
              log.debug("add in Map - key1-myMarketContext:" + myMarketContext);
              log.debug("add in Map - key2-myRequestID:" + myRequestID);
          	  log.debug("add in Map - value: " + myOadrDistributeEvent);
              
        	  
        //    crea un OadrPayload di ritorno e inserisce l'oggetto OadrDistributeEventType corrente
        //    effettua il marshal Java2MXL del payload
        //    ritorna il payload al chiamante
        	  
        	  try {
				//ret = Parser.marshal(myOadrDistributeEvent);
        		//effettua il marshal dell'oggetto myOadrDistributeEvent di tipo OadrDistributeEventType 
//      			QName qName = new QName("oadrDistributeEvent");
//    	        JAXBElement<OadrDistributeEventType> root = new JAXBElement<OadrDistributeEventType>(qName, OadrDistributeEventType.class, myOadrDistributeEvent);
        			JAXBElement<OadrDistributeEventType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrDistributeEvent(myOadrDistributeEvent);
        		  ret = Parser.marshal(root);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        log.debug("output: " + ret);
        return ret;
    }

    @POST
    @Path("oadrCreatedEvent/{marketContext}")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String postOadrEventResponse(
    		 @PathParam("marketContext") String marketContext,
    		 String oadrCreatedEvent) {
        log.debug("post postOadrEventResponse");
        log.debug("marketContext: " + marketContext);
        log.debug("oadrCreatedEvent: " + oadrCreatedEvent);
        String myMarketContext = marketContext.replaceAll("!", "/");
        String ret = null;
        log.debug("myMarketContext: " + myMarketContext);
        
        // estrae il parametro <marketContext> dal URL: marketContext
        // estrae il payload oadrCreatedEvent: oadrCreatedEvent
        
        // recupera il oadrCreatedEvent:eventResponses.eventResponse.requestID
        OadrCreatedEventType myOadrCreatedEventType = null;
        try {
        	//myOadrCreatedEventType = (OadrCreatedEventType) Parser.unmarshal(oadrCreatedEvent);
        	
        	JAXBContext jaxbContext = JAXBContext.newInstance(OadrCreatedEventType.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            JAXBElement<OadrCreatedEventType> root = jaxbUnmarshaller.unmarshal(new StreamSource(
            		new StringReader(oadrCreatedEvent)), OadrCreatedEventType.class);
        	
        	myOadrCreatedEventType = root.getValue();
        	log.debug("myOadrCreatedEventType:" + myOadrCreatedEventType);
            
            
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        log.debug("test 0 myOadrCreatedEventType:" + myOadrCreatedEventType);
        log.debug("test 1 myOadrCreatedEventType:" + myOadrCreatedEventType.getEiCreatedEvent());
        log.debug("test 2 myOadrCreatedEventType:" + myOadrCreatedEventType.getEiCreatedEvent().getEventResponses());
        log.debug("test 3 myOadrCreatedEventType:" + myOadrCreatedEventType.getEiCreatedEvent().getEventResponses().getEventResponse());
        log.debug("test 4 myOadrCreatedEventType:" + myOadrCreatedEventType.getEiCreatedEvent().getEventResponses().getEventResponse().get(0));
        log.debug("test 5 myOadrCreatedEventType:" + myOadrCreatedEventType.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getRequestID());
        String myRequestID = myOadrCreatedEventType.getEiCreatedEvent().getEventResponses().getEventResponse().get(0).getRequestID();
        log.debug("myRequestID:" + myRequestID);
        
        
        
        
        // cerca nell'hashmap la key (<marketContext>,"OadrDistributeEvent", "MAP") 
        //      (<marketContext>,"OadrDistributeEvent", "QUEUE")
        // e cerca la key(requestID)
        // se non trova l'hashmap or la coda FIFO è vuota
        ///	  ritorna una EiResponseType con HTTP code 200 OK
        
        
        if (Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext) &&
        		Oadr20bImpl.getInstance().mapMapOadrDistributeEvent != null &&
        		Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.containsKey(myMarketContext) &&
        		!Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.get(myMarketContext).isEmpty() &&
        		Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.get(myMarketContext).containsKey(myRequestID)){
       
        	//    elimina dal'hashmap l'item OadrDistributeEventType con key(requestID)
            //    crea un OadrPayload e inserisce l'oggetto oadrCreatedEventType corrente
            //    effettua il unmarshal Java2MXL del payload
            
            //  chiamata il metodo sendOadrCreatedEvent con input OadrPayload
            
          
           	Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.get(myMarketContext).remove(myRequestID);
        	//OadrCreatedEventType output = (OadrCreatedEventType) Parser.unmarshal(oadrCreatedEvent);
        	log.debug("myOadrCreatedEventType: " + myOadrCreatedEventType);
        	log.debug("myMarketContext: " + myMarketContext);
        	log.debug("mapMapOadrDistributeEvent:" + Oadr20bImpl.getInstance().mapMapOadrDistributeEvent.get(myMarketContext));
        	try {
        		Oadr20bImpl.getInstance().sendOadrCreatedEvent(myOadrCreatedEventType);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        	
        }
        // invia al chiamante risposta HTTP 200 OK
        EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType oadrResponse = new OadrResponseType();
		oadrResponse.setEiResponse(eiResponse);
		oadrResponse.setSchemaVersion(Oadr20bImpl.SCHEMA_VERSION);
		oadrResponse.setVenID(Oadr20bImpl.VEN_ID);
		try {
		//	ret = Parser.marshal(out);
//			QName qName = new QName("oadrResponse");
//	        JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrResponse(oadrResponse);
			ret = Parser.marshal(root);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ret;
    }
    
    
    @POST
    @Path("initVEN")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String initVEN() {
    	log.debug("post initVEN");
    	
        String ret = null;
        
        // esegue una QueryRegistration
        Oadr20bImpl.getInstance().doQueryRegistration();
        
        // esegue una CreatePartyRegistration
        Oadr20bImpl.getInstance().doCreatePartyRegistration();
        
        // esegue una RegisterReport
        Oadr20bImpl.getInstance().doRegisterReport();
        
        // attiva il polling
        //Oadr20bImpl.getInstance().doSetPoll();
        
        // ritorna una Response
        ret = Oadr20bImpl.getInstance().doResponse();

        return ret;
    }
    
    
    @POST
    @Path("destroyVEN")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String destroyVEN() {
    	log.debug("post destroyVEN - 7.0");
    	        
    	String ret = null;
        
    	// esegue una CancelReport
        Oadr20bImpl.getInstance().doCancelReport();
       
        // esegue una CancelPartyRegistration
        Oadr20bImpl.getInstance().doCancelPartyRegistration();
        
        // disattiva il polling
        Oadr20bImpl.getInstance().doResetPoll();
        
        // disattiva i task attivati
        Oadr20bImpl.getInstance().shutdown();
        
        // ritorna una Response
        ret = Oadr20bImpl.getInstance().doResponse();
        	
        return ret;
    }

    @GET
    @Path("oadrUpdateReport/next")
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String getOadrReportUpdate() {
        log.debug("get getOadrReportUpdate");

        String ret = null;
        
        if (Oadr20bImpl.getInstance().mapQueueOadrUpdateReport == null){
        	///	  ritorna una EiResponseType con HTTP code 200 OK
        		EiResponseType eiResponse = new EiResponseType();
        		eiResponse.setResponseCode("200");
        		eiResponse.setResponseDescription("OK");
        		OadrResponseType oadrResponse = new OadrResponseType();
        		oadrResponse.setEiResponse(eiResponse);
        		oadrResponse.setSchemaVersion(Oadr20bImpl.SCHEMA_VERSION);
        		oadrResponse.setVenID(Oadr20bImpl.VEN_ID);
        		
        		try {
					//ret = Parser.marshal(out);
        			//effettua il marshal dell'oggetto out di tipo OadrResponseType 
//        			QName qName = new QName("oadrResponse");
//        			JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
        			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrResponse(oadrResponse);
        	        ret = Parser.marshal(root);
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        } else {

        //    estrae dalla coda FIFO  un item OadrDistributeEventType
        	OadrUpdateReportType myOadrUpdateReport = Oadr20bImpl.getInstance().mapQueueOadrUpdateReport.remove();   
        //
       //
          	  log.debug("myOadrUpdateReport: " + myOadrUpdateReport);
              
        	  
        //    crea un OadrPayload di ritorno e inserisce l'oggetto OadrCreateReportType corrente
        //    effettua il marshal Java2MXL del payload
        //    ritorna il payload al chiamante
        	  
        	  try {
				//ret = Parser.marshal(myOadrDistributeEvent);
        		//effettua il marshal dell'oggetto myOadrDistributeEvent di tipo OadrDistributeEventType 
//      			QName qName = new QName("oadrCreateReport");
//    	        JAXBElement<OadrCreateReportType> root = new JAXBElement<OadrCreateReportType>(qName, OadrCreateReportType.class, myOadrCreateReport);
        		  JAXBElement<OadrUpdateReportType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrUpdateReport(myOadrUpdateReport);
        		  ret = Parser.marshal(root);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        log.debug("output: " + ret);
        return ret;
    }
    
    @GET
    @Path("oadrCreateReport/{marketContext}/next")
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String getOadrReportRequest(
    		 @PathParam("marketContext") String marketContext) {
        log.debug("get getOadrReportRequest");
        log.debug("input: " + marketContext);
        String myMarketContext = marketContext.replaceAll("!", "/");
        String ret = null;
        // estrae il parametro <marketContext> dal URL 
        
        // cerca la coda FIFO associata alla key
        //      (<marketContext>,"getOadrReportRequest", "QUEUE")
        // se non trova la coda FIFO or la coda FIFO è vuota
        
        log.debug("input: " + myMarketContext);
        
        
        if (!Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext) ||
        	Oadr20bImpl.getInstance().mapQueueOadrCreateReport == null || 
        	!Oadr20bImpl.getInstance().mapQueueOadrCreateReport.containsKey(myMarketContext) ||
        	Oadr20bImpl.getInstance().mapQueueOadrCreateReport.get(myMarketContext).isEmpty()){
        	///	  ritorna una EiResponseType con HTTP code 200 OK
        		EiResponseType eiResponse = new EiResponseType();
        		eiResponse.setResponseCode("200");
        		eiResponse.setResponseDescription("OK");
        		OadrResponseType oadrResponse = new OadrResponseType();
        		oadrResponse.setEiResponse(eiResponse);
        		oadrResponse.setSchemaVersion(Oadr20bImpl.SCHEMA_VERSION);
        		oadrResponse.setVenID(Oadr20bImpl.VEN_ID);
        		
        		try {
					//ret = Parser.marshal(out);
        			//effettua il marshal dell'oggetto out di tipo OadrResponseType 
//        			QName qName = new QName("oadrResponse");
//        			JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
        			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrResponse(oadrResponse);
        	        ret = Parser.marshal(root);
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        } else {
        	log.debug("get mapQueueOadrCreateReport: " + Oadr20bImpl.getInstance().mapQueueOadrCreateReport.get(myMarketContext));
        // altrimenti (coda trovata)
        //    estrae dalla coda FIFO  un item OadrDistributeEventType
        	OadrCreateReportType myOadrCreateReport = Oadr20bImpl.getInstance().mapQueueOadrCreateReport.get(myMarketContext).remove();   
        //
        //    recupera il oadrPayload:oadrSignedObject:oadrDistributeEvent:requestID
        //
        	  String myReportRequestID = myOadrCreateReport.getOadrReportRequest().get(0).getReportRequestID();
        //    se non esiste la coda con key (<marketContext>,"OadrCreateReportType", "MAP") 
        // 		 crea una hashmap
        //    altrimenti (hashmap trovata)
        //      inserisce l'item OadrCreateReportType corrente nell'hashmap
        //      utilizzando come key(myReportRequestID)
        //    
        	  if (Oadr20bImpl.getInstance().mapMapOadrCreateReport == null) {
        		  Oadr20bImpl.getInstance().mapMapOadrCreateReport = new HashMap<String, Map<String, OadrCreateReportType>>();
        		                                                       
        	  }		
              if (!Oadr20bImpl.getInstance().mapMapOadrCreateReport.containsKey(myMarketContext)) { 	
            	 HashMap<String, OadrCreateReportType> mapOadrCreateReportType = new HashMap<String, OadrCreateReportType>();
            	  Oadr20bImpl.getInstance().mapMapOadrCreateReport.put(myMarketContext,mapOadrCreateReportType);
              }
              
              Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).put(myReportRequestID, myOadrCreateReport);   
              log.debug("add in Map - key1:" + myMarketContext);
              log.debug("add in Map - key2:" + myReportRequestID);
          	  log.debug("add in Map - value: " + myOadrCreateReport);
              
        	  
        //    crea un OadrPayload di ritorno e inserisce l'oggetto OadrCreateReportType corrente
        //    effettua il marshal Java2MXL del payload
        //    ritorna il payload al chiamante
        	  
        	  try {
				//ret = Parser.marshal(myOadrDistributeEvent);
        		//effettua il marshal dell'oggetto myOadrDistributeEvent di tipo OadrDistributeEventType 
//      			QName qName = new QName("oadrCreateReport");
//    	        JAXBElement<OadrCreateReportType> root = new JAXBElement<OadrCreateReportType>(qName, OadrCreateReportType.class, myOadrCreateReport);
        		  JAXBElement<OadrCreateReportType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrCreateReport(myOadrCreateReport);
        		  ret = Parser.marshal(root);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        log.debug("output: " + ret);
        return ret;
    }
    
    @POST
    @Path("oadrUpdateReport/{marketContext}") 
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public synchronized String postOadrUpdateReport(
    		 @PathParam("marketContext") String marketContext,
    //		 String oadrUpdateReport) {
  		 String oadrPayload) {
        log.debug("post postOadrUpdateReport");
        log.debug("marketContext: " + marketContext);
        //log.debug("oadrUpdateReport: " + oadrUpdateReport);
        log.debug("oadrUpdateReport: " + oadrPayload);
        String myMarketContext = marketContext.replaceAll("!", "/");
        String ret = null;
        log.debug("myMarketContext: " + myMarketContext);
        
        // estrae il parametro <marketContext> dal URL: marketContext
        // estrae il payload oadrUpdateReport: oadrUpdateReport
        
//        OadrUpdateReportType myOadrUpdateReportType = null;
//        try {
//        	
//        	//JAXBContext jaxbContext = JAXBContext.newInstance(OadrUpdateReportType.class);
//             JAXBContext jaxbContext = JAXBContext.newInstance(
//                                "ietf.params.xml.ns.icalendar_2:"
//                              + "ietf.params.xml.ns.icalendar_2_0.stream:"
//                              + "net.opengis.gml._3:"
//                              + "org.naesb.espi:"
//                              + "org.oasis_open.docs.ns.emix._2011._06:"
//                              + "org.oasis_open.docs.ns.emix._2011._06.power:"
//                              + "org.oasis_open.docs.ns.emix._2011._06.siscale:"
//                              + "org.oasis_open.docs.ns.energyinterop._201110:"
//                              + "org.oasis_open.docs.ns.energyinterop._201110.payloads:"
//                              + "org.openadr.oadr_2_0b._2012._07:"
//                              + "org.w3._2000._09.xmldsig_:"
//                              + "org.w3._2005.atom:"
//                              + "org.w3._2009.xmldsig11_:"
//                              + "un.unece.uncefact.codelist.standard._5.iso42173a._2010_04_07");
//        	
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//            JAXBElement<OadrUpdateReportType> root = jaxbUnmarshaller.unmarshal(new StreamSource(
//            		new StringReader(oadrUpdateReport)), OadrUpdateReportType.class);
//        	
//            myOadrUpdateReportType = root.getValue();
//        	log.debug("myOadrUpdateReportType:" + myOadrUpdateReportType);
//            
//            
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

        OadrPayload in = null;
        try {
            in = (OadrPayload)Parser.unmarshal(oadrPayload);
        } catch (JAXBException ex) {
            log.error(ex.getMessage(), ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        OadrSignedObject so = in.getOadrSignedObject();
        log.debug("test 0 OadrSignedObject:" + so);
        OadrUpdateReportType myOadrUpdateReportType = so.getOadrUpdateReport();
        log.debug("test 1 myOadrUpdateReportType:" + myOadrUpdateReportType);
        log.debug("test 2 getOadrReport:" + myOadrUpdateReportType.getOadrReport());
        log.debug("test 3 myOadrUpdateReportType:" + myOadrUpdateReportType.getOadrReport().getReportRequestID());
        log.debug("test 4 myOadrUpdateReportType:" + myOadrUpdateReportType.getOadrReport().getReportRequestID());
        
        //log.debug("test 0 myOadrUpdateReportType:" + myOadrUpdateReportType);
        //log.debug("test 1 getOadrReport:" + myOadrUpdateReportType.getOadrReport());
        //log.debug("test 2 myOadrUpdateReportType:" + myOadrUpdateReportType.getOadrReport().get(0));
        //log.debug("test 3 myOadrUpdateReportType:" + myOadrUpdateReportType.getOadrReport().get(0).getReportRequestID());
        //log.debug("test 3 myOadrUpdateReportType:" + myOadrUpdateReportType.getOadrReport().getReportRequestID());
        //String myReportRequestID = myOadrUpdateReportType.getOadrReport().get(0).getReportRequestID();
        String myReportRequestID = myOadrUpdateReportType.getOadrReport().getReportRequestID();
        log.debug("myReportRequestID:" + myReportRequestID);
        
        // cerca nell'hashmap la key (<marketContext>,"OadrCreateReportType", "MAP") 
        //      (<marketContext>,"OadrCreateReportType", "QUEUE")
        // e cerca la key(myReportRequestID)
        // se non trova l'hashmap or la coda FIFO è vuota
        ///	  ritorna una EiResponseType con HTTP code 200 OK
        
        log.debug("test 10: " + Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext));
        log.debug("test 11: " +  (Oadr20bImpl.getInstance().mapMapOadrCreateReport != null));
        log.debug("test 12: " + (Oadr20bImpl.getInstance().mapMapOadrCreateReport.containsKey(myMarketContext)));
        log.debug("test 13: " + (!Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).isEmpty()));
        log.debug("test 14: " + (Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).containsKey(myReportRequestID)));
        
        if (Oadr20bImpl.getInstance().LIST_MARKET_CONTEXT.contains(myMarketContext) &&
            Oadr20bImpl.getInstance().mapMapOadrCreateReport != null &&
        	Oadr20bImpl.getInstance().mapMapOadrCreateReport.containsKey(myMarketContext) &&
        	!Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).isEmpty() &&
        	Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).containsKey(myReportRequestID)){
       
        	//  crea un OadrPayload e inserisce l'oggetto OadrCreateReportType corrente
            //  effettua il unmarshal Java2MXL del payload
            //  chiamata il metodo sendOadrCreatedEvent con input OadrPayload
   
        	
           	Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext).remove(myReportRequestID);
        	
        	log.debug("myOadrUpdateReportType: " + myOadrUpdateReportType);
        	log.debug("myMarketContext: " + myMarketContext);
        	log.debug("mapMapOadrCreateReport:" + Oadr20bImpl.getInstance().mapMapOadrCreateReport.get(myMarketContext));
        	if(Oadr20bImpl.getInstance().LIST_REPORT_REQUEST_ID.contains(myReportRequestID)){
        		try {
            		Oadr20bImpl.getInstance().sendOadrUpdateReport(myOadrUpdateReportType);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
        	}
        	
        }
        
        // invia al chiamante risposta HTTP 200 OK
        EiResponseType eiResponse = new EiResponseType();
		eiResponse.setResponseCode("200");
		eiResponse.setResponseDescription("OK");
		OadrResponseType oadrResponse = new OadrResponseType();
		oadrResponse.setEiResponse(eiResponse);
		oadrResponse.setSchemaVersion(Oadr20bImpl.SCHEMA_VERSION);
		oadrResponse.setVenID(Oadr20bImpl.VEN_ID);
		try {
		//	ret = Parser.marshal(out);
//			QName qName = new QName("oadrResponse");
//	        JAXBElement<OadrResponseType> root = new JAXBElement<OadrResponseType>(qName, OadrResponseType.class, oadrResponse);
			JAXBElement<OadrResponseType> root = new org.openadr.oadr_2_0b._2012._07.ObjectFactory().createOadrResponse(oadrResponse);
			ret = Parser.marshal(root);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ret;
    }
 
}
