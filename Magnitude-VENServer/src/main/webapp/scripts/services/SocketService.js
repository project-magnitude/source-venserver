/*globals io*/

(function (window, angular) {
    'use strict';

    angular.module('Services')
    .factory('Socket', [
        '$rootScope', '$location', 'BASE_PATH', 'SOCKET_PORT',
        function ($rootScope, $location, basePath, socketPort) {
            var protocol = $location.protocol(); // ws
            var host = $location.host();
            var port = $location.port();
            port = socketPort;
            var basehost = protocol + '://' + host + ':' + port;
            var path = basePath;

            var socket = io.connect(basehost, {
                // transports: ['websocket'],
                // resource: '/VENServer/socket.io',
                // path: path, // + '/socket.io',
                forceNew: true
            });
            return {
                on: function (eventName, callback) {
                    socket.on(eventName, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            callback.apply(socket, args);
                        });
                    });
                },
                emit: function (eventName, data, callback) {
                    socket.emit(eventName, data, function () {
                        var args = arguments;
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback.apply(socket, args);
                            }
                        });
                    });
                }
            };
        }
    ]);
}(this, angular));
