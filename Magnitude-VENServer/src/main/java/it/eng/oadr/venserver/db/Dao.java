package it.eng.oadr.venserver.db;

import java.util.List;

import javax.persistence.TypedQuery;

import it.eng.oadr.venserver.db.model.AbstractEntity;
import it.eng.oadr.venserver.db.model.Event;

public class Dao<E extends AbstractEntity> implements IDao<E, Long> {
	// Cannot pass E.class to the method.
	// Workaround it.
	private Class<E> clazz;

	public Dao(Class<E> clazz) {
		this.clazz = clazz;
	}

	@Override
	public void persist(E entity) {
		EntityManagerHelper.beginTransaction();
		EntityManagerHelper.getEntityManager().persist(entity);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
	}

	@Override
	public void remove(E entity) {
		EntityManagerHelper.beginTransaction();
		EntityManagerHelper.getEntityManager().remove(entity);
		EntityManagerHelper.commit();
		EntityManagerHelper.closeEntityManager();
	}

	@Override
	public E findById(Long id) {
		E entity = EntityManagerHelper.getEntityManager().find(clazz, id);
		EntityManagerHelper.closeEntityManager();
		return entity;
	}
	
	public List<E> getAll(Integer start, Integer end) {
		TypedQuery<E> findAllQuery = EntityManagerHelper.getEntityManager().createQuery(
				"SELECT DISTINCT m FROM " + clazz.getSimpleName() + " m ORDER BY m.id", clazz);
		if (start != null) {
			findAllQuery.setFirstResult(start);
		}
		if (end != null) {
			findAllQuery.setMaxResults(end);
		}
		final List<E> results = findAllQuery.getResultList();
		EntityManagerHelper.closeEntityManager();
		return results;
	}
	
	public E removeById(Long id){
		E entity = EntityManagerHelper.getEntityManager().find(clazz, id);
		if(entity != null){
			EntityManagerHelper.beginTransaction();
			EntityManagerHelper.getEntityManager().remove(entity);
			EntityManagerHelper.commit();
		}
		EntityManagerHelper.closeEntityManager();
		return entity;
	}
	
	public E updateById(E entity, Long id){
		E res = EntityManagerHelper.getEntityManager().find(clazz, id);
		if(res != null){
			EntityManagerHelper.beginTransaction();
			res = EntityManagerHelper.getEntityManager().merge(entity);
			EntityManagerHelper.commit();
		}
		EntityManagerHelper.closeEntityManager();
		return res;
	}
	
	public List<E> findByField(String field, Object value){
		TypedQuery<E> query = EntityManagerHelper.getEntityManager().createQuery(
				"FROM "+clazz.getSimpleName()+" m WHERE m."+field+" = :"+field, clazz);
		query.setParameter(field, value);
		List<E> list = query.getResultList();
		EntityManagerHelper.closeEntityManager();
		return list;
	}
}
