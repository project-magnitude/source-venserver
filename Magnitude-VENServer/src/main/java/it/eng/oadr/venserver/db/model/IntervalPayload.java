package it.eng.oadr.venserver.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class IntervalPayload  extends AbstractEntity implements Serializable{
	private static final long serialVersionUID = 8826817600128286682L;
	
	@Column(name="uid")
	private int uid;
	
	@Column(name="value")
	private float value;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "signal_id")
	private Signal signal;

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public Signal getSignal() {
		return signal;
	}

	public void setSignal(Signal signal) {
		this.signal = signal;
	}
}
