(function (window, angular) {
    'use strict';

    angular.module('venUI', [
        // Libraries.
        'ngAnimate',
        'ngResource',
        'ngTagsInput',
        'ngTouch',
        'ui.bootstrap',
        'ui.router',

        // Threated as modules even if they are in the project.
        'flash',

        // Modules.
        'appRoutes',
        'Services',
        'Directives',
        'Controllers'
    ])
    .config([
        '$httpProvider',
        function ($httpProvider) {
            $httpProvider.useLegacyPromiseExtensions(false);
        }
    ])
    .constant('BASE_PATH', '/venserver')
    .constant('SOCKET_PORT', 8084);
}(this, angular));
