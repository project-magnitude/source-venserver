(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('EventController', [
        '$state', 'EventService', 'Socket', '$timeout', 'flash',
        function ($state, EventService, Socket, $timeout, flash) {
            var self = this;
            self.events = [];
            
            self.$onInit = function(){
            	self.events = EventService.queryAll();
            };
            
            self.showSignals = function (event) {
                function successCallback(data) {
                    self.event = data;
                    for(var i=0;i<self.event.signals.length;i++){
                    	var signal = self.event.signals[i];
                    	var values = [];
                    	for(var j=0;j<signal.intervals.length;j++){
                    		values.push(signal.intervals[j].value);
                    	}
                    	signal.intervals = values.toString();
                    }
                }
                function errorCallback(error) {
                    flash.setError('The event could not be found.');
                }
                EventService.get({ id: event.id }, successCallback, errorCallback);
            };

            Socket.on('event', function (data) {
                $timeout(function () {
                    self.events.push(data);
                });
            });
        }
    ]);
}(this, angular));