(function (window, angular) {
    'use strict';

    angular.module('flash', [])
    .factory('flash', [
        '$rootScope', '$log',
        function ($rootScope, $log) {
            var messages = [];
            var currentMessage = {};

            $rootScope.$on('$stateChangeSuccess', function () {
                currentMessage = messages.shift() || {};
            });

            return {
                getMessage: function () {
                    return currentMessage;
                },
                setMessage: function (message, pop) {
                    switch (message.type) {
                        case 'error':
                            message.cssClass = 'danger';
                            break;
                        case 'success':
                            message.cssClass = 'success';
                            break;
                        case 'info':
                            message.cssClass = 'info';
                            break;
                        case 'warning':
                            message.cssClass = 'warning';
                            break;
                        default:
                            $log.warn('Unknown message type: ' + message.type);
                            message.cssClass = 'warning';
                            break;
                    }
                    messages.push(message);
                    if (pop) {
                        currentMessage = messages.shift() || {};
                    }
                },
                setSuccess: function (text, pop) {
                    var message = {
                        text: text,
                        type: 'success'
                    };
                    this.setMessage(message, pop);
                },
                setError: function (text, pop) {
                    var message = {
                        text: text,
                        type: 'error'
                    };
                    this.setMessage(message, pop);
                }
            };
    }]);
}(this, angular));
