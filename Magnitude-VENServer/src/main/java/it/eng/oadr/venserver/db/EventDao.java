package it.eng.oadr.venserver.db;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.venserver.db.model.Event;

public class EventDao extends Dao<Event>{

	private static final Logger log = LoggerFactory.getLogger(EventDao.class);

	public EventDao(){
		super(Event.class);
	}

	@Override
	public Event findById(Long id) {
		Event res = null;
		TypedQuery<Event> query = EntityManagerHelper.getEntityManager().createQuery(
				"FROM Event m JOIN FETCH m.signals s WHERE m.id = :id", Event.class);
		query.setParameter("id", id);
		List<Event> list = query.getResultList();
		
		if(!list.isEmpty()){
			res = list.get(0);
		}
		EntityManagerHelper.closeEntityManager();
		return res;
	}
	
	@Override
	public List<Event> getAll(Integer start, Integer end) {
		TypedQuery<Event> findAllQuery = EntityManagerHelper.getEntityManager().createQuery(
				"SELECT new Event(m.id, m.eventId, m.startTime, m.createdDate, m.duration, m.eventStatus, m.marketContext) FROM Event m ORDER BY m.id", Event.class);
		if (start != null) {
			findAllQuery.setFirstResult(start);
		}
		if (end != null) {
			findAllQuery.setMaxResults(end);
		}
		final List<Event> results = findAllQuery.getResultList();
		EntityManagerHelper.closeEntityManager();
		return results;
	}
}
