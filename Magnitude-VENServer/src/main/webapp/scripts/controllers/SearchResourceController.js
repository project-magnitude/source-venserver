(function (window, angular) {
    'use strict';

    angular.module('Controllers')
    .controller('SearchResourceController', [
        '$filter', 'ResourceService',
        function ($filter, ResourceService) {
            var self = this;

            self.search = {};
            self.currentPage = 0;
            self.pageSize = 10;
            self.searchResults = [];
            self.filteredResults = [];
            self.pageRange = [];

            self.numberOfPages = function () {
                var result = Math.ceil(self.filteredResults.length / self.pageSize);
                var max = (result === 0) ? 1 : result;
                self.pageRange = [];
                for (var ctr = 0; ctr < max; ctr++) {
                    self.pageRange.push(ctr);
                }
                return max;
            };

            self.performSearch = function () {
                self.searchResults = ResourceService.queryAll(function () {
                    self.filteredResults = $filter('searchFilter')(self.searchResults, self);
                    self.currentPage = 0;
                });
            };

            self.previous = function () {
                if (self.currentPage > 0) {
                    self.currentPage--;
                }
            };

            self.next = function () {
                if (self.currentPage < (self.numberOfPages() - 1)) {
                    self.currentPage++;
                }
            };

            self.setPage = function(n) {
                self.currentPage = n;
            };

            self.performSearch();
        }
    ]);
}(this, angular));
