(function (window, angular) {
    'use strict';

    angular.module('appRoutes', [])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        '$locationProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $stateProvider
            .state('root', {
                url: '/',
                onEnter: [
                    '$state', '$timeout',
                    function ($state, $timeout) {
                        // The timeout is needed to prevent some errors with the ui-router.
                        // Related: https://github.com/angular-ui/ui-router/issues/1901
                        $timeout(function () {
                            $state.go('registration');
                        });
                    }
                ]
            })
            .state('registration', {
                url: '/registration',
                templateUrl: 'views/registration.html',
                controller: 'RegistrationController',
                controllerAs: 'registrationCtrl'
            })
            .state('resources', {
                url: '/resources',
                abstract: true,
                template: '<div ui-view></div>'
            })
            .state('resources.search', {
                url: '/search',
                templateUrl: 'views/resources/search.html',
                controller: 'SearchResourceController',
                controllerAs: 'searchResourceCtrl'
            })
            .state('resources.detail', {
                url: '/detail/:id',
                templateUrl: 'views/resources/detail.html',
                controller: 'ResourceController',
                controllerAs: 'resourceCtrl'
            })
            .state('reports', {
                url: '/reports',
                templateUrl: 'views/reports.html',
                controller: 'ReportController',
                controllerAs: 'reportCtrl'
            })
            .state('events', {
                url: '/events',
                templateUrl: 'views/events.html',
                controller: 'EventController',
                controllerAs: 'eventCtrl'
            });

            $urlRouterProvider.otherwise('/');
            // $locationProvider.html5Mode(true);
        }
    ]);
}(this, angular));
