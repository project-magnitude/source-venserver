package it.eng.oadr.venserver.impl;

import java.util.TimerTask;

import org.openadr.oadr_2_0b._2012._07.OadrPollType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PeriodicTask extends TimerTask{

	private static final Logger log = LoggerFactory.getLogger(PeriodicTask.class);
	@Override
	public void run() {
		// crea un oggetto OadrPollType
		OadrPollType oPo = new OadrPollType();
	    // imposta i campi di OadrPollType
        // utilizza un ID di 10 cifre esadecimali
		oPo.setVenID(Oadr20bImpl.getInstance().VEN_ID);
        // imposta lo schema Version
		oPo.setSchemaVersion(Oadr20bImpl.getInstance().SCHEMA_VERSION);
        // invia  l'oggetto OadrPollType
        
        try {
        	
        	//log.debug("oPo: " + oPo);
        	Oadr20bImpl.getInstance().sendOadrPoll(oPo);
        	        	
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
	}

}
