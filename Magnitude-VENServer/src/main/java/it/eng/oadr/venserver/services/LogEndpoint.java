package it.eng.oadr.venserver.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.eng.oadr.venserver.db.model.Log;
import it.eng.oadr.venserver.impl.Oadr20bImpl;

@Path("log")
public class LogEndpoint extends AbstractEndpoint<Log> {
	
	private static final Logger logger = LoggerFactory.getLogger(LogEndpoint.class);
	
	public LogEndpoint() {
		super(Log.class);
	}

	@GET
	@Path("/polling-state")
	@Produces("application/json")
	public Response isPollRunning() {
		logger.debug("-->isPollRunning");
		boolean isPollRunning = Oadr20bImpl.getInstance().isPollRunning();
		logger.debug("isPollRunning: " + isPollRunning);
		Boolean entity = Boolean.valueOf(isPollRunning);
		logger.debug("entity: " + entity);
		return Response.ok(entity).build();
	}

}
