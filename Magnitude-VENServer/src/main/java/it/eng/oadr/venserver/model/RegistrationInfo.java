package it.eng.oadr.venserver.model;

import java.util.List;

public class RegistrationInfo {
	public String venUrl;
	public String venName;
	public String venId;
	public String target;
	public List<String> marketContexts;

	public String getVenUrl() {
		return venUrl;
	}
	public void setVenUrl(String url) {
		this.venUrl = url;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getVenName() {
		return venName;
	}
	public void setVenName(String venName) {
		this.venName = venName;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public List<String> getMarketContexts() {
		return marketContexts;
	}
	public void setMarketContexts(List<String> marketContexts) {
		this.marketContexts = marketContexts;
	}
}
